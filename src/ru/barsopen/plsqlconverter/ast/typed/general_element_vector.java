package ru.barsopen.plsqlconverter.ast.typed;
public class general_element_vector implements expression_element, _baseNode {
  public int _line = -1;
  public int _col = -1;
  public int _tokenStartIndex = -1;
  public int _tokenStopIndex = -1;
  public _baseNode _parent = null;
  public _baseNode _getParent() { return _parent; }
  public void _setParent(_baseNode value) { _parent = value; }
  public void _setBaseNode(_baseNode value) { this._parent = value; }
  public int _getLine() { return _line; }
  public int _getCol() { return _col; }
  public int _getTokenStartIndex() { return _tokenStartIndex; }
  public int _getTokenStopIndex() { return _tokenStopIndex; }
ru.barsopen.plsqlconverter.util.AttachedComments _comments;
public void setComments(ru.barsopen.plsqlconverter.util.AttachedComments comments) { this._comments = comments; }
public ru.barsopen.plsqlconverter.util.AttachedComments getAttachedComments() { return this._comments; }
public void _setCol(int col) { this._col = col; }
public void _setLine(int line) { this._line = line; }

  public java.util.List<general_expression> general_expressions = new java.util.ArrayList<general_expression>();
  public java.util.List<general_expression> get_general_expressions() { return this.general_expressions; }
  public void add_general_expressions(general_expression value) {
    insert_general_expressions(general_expressions.size(), value);
  }
  public void insert_general_expressions(int pos, general_expression value) {
    this.general_expressions.add(pos, value);
    value._setParent(this);
  }
  public void remove_general_expressions(int pos) {
    this.general_expressions.get(pos)._setParent(null);
    this.general_expressions.remove(pos);
  }
  public void remove_general_expressions(general_expression value) {
    this.remove_general_expressions(this.general_expressions.indexOf(value));
  }

  public void _walk(_visitor visitor) {
    if (!visitor.enter(this)) {
      return;
    }
    for (general_expression _value: this.general_expressions) {
      _value._walk(visitor);
    }
    visitor.leave(this);
  }

  public java.util.List<_baseNode> _getChildren() {
    java.util.List<_baseNode> result = new java.util.ArrayList<_baseNode>();
    result.addAll(this.general_expressions);
    return result;
  }

  public void _replace(_baseNode child, _baseNode replacement) {
    for (int _i = 0; _i < this.general_expressions.size(); ++_i) {
      if (this.general_expressions.get(_i) == child) {
        this.remove_general_expressions(_i);
        this.insert_general_expressions(_i, (ru.barsopen.plsqlconverter.ast.typed.general_expression)replacement);
        return;
      }
    }
    throw new RuntimeException("ErrorMessage:Failed to replace node: no such node. <br>ParentPosition:" + _line + ":" + _col + "<br>ChildPosition:" + child._getLine() + ":" + child._getCol());
  }

  public org.antlr.runtime.tree.Tree unparse() {
    org.antlr.runtime.CommonToken _token = new org.antlr.runtime.CommonToken(ru.barsopen.plsqlconverter.PLSQLPrinter.VECTOR_EXPR);
    _token.setLine(_line);
    _token.setCharPositionInLine(_col);
    _token.setText("VECTOR_EXPR");
    org.antlr.runtime.tree.CommonTree _result = new org.antlr.runtime.tree.CommonTree(_token);

if (_comments != null) {
  org.antlr.runtime.tree.CommonTree commentsNode = new org.antlr.runtime.tree.CommonTree(
    new org.antlr.runtime.CommonToken(ru.barsopen.plsqlconverter.PLSQLPrinter.COMMENT));
  org.antlr.runtime.tree.CommonTree commentsBeforeNode = new org.antlr.runtime.tree.CommonTree(
    new org.antlr.runtime.CommonToken(ru.barsopen.plsqlconverter.PLSQLPrinter.COMMENT, _comments.getBefore()));
  org.antlr.runtime.tree.CommonTree commentsAfterNode = new org.antlr.runtime.tree.CommonTree(
    new org.antlr.runtime.CommonToken(ru.barsopen.plsqlconverter.PLSQLPrinter.COMMENT, _comments.getAfter()));
  org.antlr.runtime.tree.CommonTree commentsInsideNode = new org.antlr.runtime.tree.CommonTree(
    new org.antlr.runtime.CommonToken(ru.barsopen.plsqlconverter.PLSQLPrinter.COMMENT, _comments.getInside()));
  commentsNode.addChild(commentsBeforeNode);
  commentsNode.addChild(commentsInsideNode);
  commentsNode.addChild(commentsAfterNode);
  _result.addChild(commentsNode);
}

    _result.setTokenStartIndex(_tokenStartIndex);
    _result.setTokenStopIndex(_tokenStopIndex);
    if (general_expressions.size() == 0) { throw new RuntimeException(); }
    for (int i = 0; i < general_expressions.size(); ++i) {
      _result.addChild(general_expressions.get(i).unparse());
    }


    return _result;
  }

}
