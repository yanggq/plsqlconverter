package ru.barsopen.plsqlconverter.ast.typed;
public class expression_element_interval implements expression_element, _baseNode {
  public int _line = -1;
  public int _col = -1;
  public int _tokenStartIndex = -1;
  public int _tokenStopIndex = -1;
  public _baseNode _parent = null;
  public _baseNode _getParent() { return _parent; }
  public void _setParent(_baseNode value) { _parent = value; }
  public void _setBaseNode(_baseNode value) { this._parent = value; }
  public int _getLine() { return _line; }
  public int _getCol() { return _col; }
  public int _getTokenStartIndex() { return _tokenStartIndex; }
  public int _getTokenStopIndex() { return _tokenStopIndex; }
ru.barsopen.plsqlconverter.util.AttachedComments _comments;
public void setComments(ru.barsopen.plsqlconverter.util.AttachedComments comments) { this._comments = comments; }
public ru.barsopen.plsqlconverter.util.AttachedComments getAttachedComments() { return this._comments; }
public void _setCol(int col) { this._col = col; }
public void _setLine(int line) { this._line = line; }

  public constant_char_string constant_char_string = null;
  public constant_char_string get_constant_char_string() { return this.constant_char_string; }
  public void set_constant_char_string(constant_char_string value) {
    if (this.constant_char_string != null) { this.constant_char_string._setParent(null); }
    this.constant_char_string = value;
    if (this.constant_char_string != null) { this.constant_char_string._setParent(this); }
  }
  public interval_function_first_day interval_function_first_day = null;
  public interval_function_first_day get_interval_function_first_day() { return this.interval_function_first_day; }
  public void set_interval_function_first_day(interval_function_first_day value) {
    if (this.interval_function_first_day != null) { this.interval_function_first_day._setParent(null); }
    this.interval_function_first_day = value;
    if (this.interval_function_first_day != null) { this.interval_function_first_day._setParent(this); }
  }
  public interval_function_second_second interval_function_second_second = null;
  public interval_function_second_second get_interval_function_second_second() { return this.interval_function_second_second; }
  public void set_interval_function_second_second(interval_function_second_second value) {
    if (this.interval_function_second_second != null) { this.interval_function_second_second._setParent(null); }
    this.interval_function_second_second = value;
    if (this.interval_function_second_second != null) { this.interval_function_second_second._setParent(this); }
  }

  public void _walk(_visitor visitor) {
    if (!visitor.enter(this)) {
      return;
    }
    if (this.constant_char_string != null) {
      this.constant_char_string._walk(visitor);
    }
    if (this.interval_function_first_day != null) {
      this.interval_function_first_day._walk(visitor);
    }
    if (this.interval_function_second_second != null) {
      this.interval_function_second_second._walk(visitor);
    }
    visitor.leave(this);
  }

  public java.util.List<_baseNode> _getChildren() {
    java.util.List<_baseNode> result = new java.util.ArrayList<_baseNode>();
    if (this.constant_char_string != null) {
      result.add(this.constant_char_string);
    }
    if (this.interval_function_first_day != null) {
      result.add(this.interval_function_first_day);
    }
    if (this.interval_function_second_second != null) {
      result.add(this.interval_function_second_second);
    }
    return result;
  }

  public void _replace(_baseNode child, _baseNode replacement) {
    if (this.constant_char_string == child) {
      this.set_constant_char_string((ru.barsopen.plsqlconverter.ast.typed.constant_char_string)replacement);
      return;
    }
    if (this.interval_function_first_day == child) {
      this.set_interval_function_first_day((ru.barsopen.plsqlconverter.ast.typed.interval_function_first_day)replacement);
      return;
    }
    if (this.interval_function_second_second == child) {
      this.set_interval_function_second_second((ru.barsopen.plsqlconverter.ast.typed.interval_function_second_second)replacement);
      return;
    }
    throw new RuntimeException("Failed to replace node: no such node");
  }

  public org.antlr.runtime.tree.Tree unparse() {
    org.antlr.runtime.CommonToken _token = new org.antlr.runtime.CommonToken(ru.barsopen.plsqlconverter.PLSQLPrinter.INTERVAL_DATATYPE);
    _token.setLine(_line);
    _token.setCharPositionInLine(_col);
    _token.setText("INTERVAL_DATATYPE");
    org.antlr.runtime.tree.CommonTree _result = new org.antlr.runtime.tree.CommonTree(_token);

if (_comments != null) {
  org.antlr.runtime.tree.CommonTree commentsNode = new org.antlr.runtime.tree.CommonTree(
    new org.antlr.runtime.CommonToken(ru.barsopen.plsqlconverter.PLSQLPrinter.COMMENT));
  org.antlr.runtime.tree.CommonTree commentsBeforeNode = new org.antlr.runtime.tree.CommonTree(
    new org.antlr.runtime.CommonToken(ru.barsopen.plsqlconverter.PLSQLPrinter.COMMENT, _comments.getBefore()));
  org.antlr.runtime.tree.CommonTree commentsAfterNode = new org.antlr.runtime.tree.CommonTree(
    new org.antlr.runtime.CommonToken(ru.barsopen.plsqlconverter.PLSQLPrinter.COMMENT, _comments.getAfter()));
  org.antlr.runtime.tree.CommonTree commentsInsideNode = new org.antlr.runtime.tree.CommonTree(
    new org.antlr.runtime.CommonToken(ru.barsopen.plsqlconverter.PLSQLPrinter.COMMENT, _comments.getInside()));
  commentsNode.addChild(commentsBeforeNode);
  commentsNode.addChild(commentsInsideNode);
  commentsNode.addChild(commentsAfterNode);
  _result.addChild(commentsNode);
}

    _result.setTokenStartIndex(_tokenStartIndex);
    _result.setTokenStopIndex(_tokenStopIndex);
    if (constant_char_string == null) { throw new RuntimeException(); }
    _result.addChild(constant_char_string.unparse());


    if (interval_function_first_day == null) { throw new RuntimeException(); }
    _result.addChild(interval_function_first_day.unparse());


    if (interval_function_second_second == null) { throw new RuntimeException(); }
    _result.addChild(interval_function_second_second.unparse());


    return _result;
  }

}
