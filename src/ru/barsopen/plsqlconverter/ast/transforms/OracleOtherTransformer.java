/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.barsopen.plsqlconverter.ast.transforms;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import ru.barsopen.plsqlconverter.PLSQLPrinter;
import ru.barsopen.plsqlconverter.ast.typed.*;
import ru.barsopen.plsqlconverter.util.ReflectionUtil;

/**
 *
 * @author yanggq
 */
public class OracleOtherTransformer {
	private static Logger logger = LogManager.getLogger(OracleOtherTransformer.class);
    public static void deleteCommitInfo(_baseNode tree) throws Exception {
        List<commit_statement> commitList = AstUtil.getDescendantsOfType(tree, commit_statement.class);
        
        for(commit_statement commit : commitList) {
            _baseNode b = commit._parent;
            
            if(b instanceof seq_of_statements) {
                ((seq_of_statements) b).remove_stat_or_labels(commit);
            }
        }
    }
    
    public static void deleteRownumGreaterAndEqual(logic_expression logicExpr){
        List<expression_element_gte> gtes = AstUtil.getDescendantsOfType(logicExpr, expression_element_gte.class);
        
        List<expression_element_gte> list = new ArrayList<>();
        for(expression_element_gte gte : gtes) {
            try {
                String val = ((general_element_id)((general_element)gte.lhs).general_element_items.get(0)).id.value;
                if("rownum".equalsIgnoreCase(val)) {
                    list.add(gte);
                }
            } catch(Exception e) {   
            }
        }
        
        for(expression_element_gte gte: list) {
            _baseNode parent = gte._getParent();
            
            if(parent.equals(logicExpr)) {
                //delete where_clause from query
                _baseNode where  = parent._getParent();
                _baseNode query = where._getParent();
                query._replace(where, null);
                return;
            }
            
            _baseNode parent2  = parent._getParent();
            
            //get brother node
            expression_element expression = null;
            if(parent instanceof expression_element_and) {
                if(((expression_element_and) parent).lhs.equals(gte)) {
                    expression = ((expression_element_and) parent).rhs;
                } else {
                    expression = ((expression_element_and) parent).lhs;
                }
            } else if(parent instanceof expression_element_or) {
                if(((expression_element_or) parent).lhs.equals(gte)) {
                    expression = ((expression_element_or) parent).rhs;
                } else {
                    expression = ((expression_element_or) parent).lhs;
                }
            }
            if(expression == null) {
                continue;
            }
            
            //delete parent note
            //put the brother node to parent of parent
            if(parent2 instanceof expression_element_and) {
                if(((expression_element_and) parent2).lhs.equals(parent)) {
                    ((expression_element_and) parent2).lhs = expression;
                } else {
                    ((expression_element_and) parent2).rhs = expression;
                }
            } else  if(parent2 instanceof expression_element_or) {
                if(((expression_element_or) parent2).lhs == parent) {
                    ((expression_element_or) parent2).lhs = expression;
                } else {
                    ((expression_element_or) parent2).rhs = expression;
                }
            } else {
                logicExpr.expression_element = expression;
            }
        }
    }
    
    private static void changeQueryBlockRownum(query_block query, _baseNode node){
        try {
                
                if(query.where_clause != null) {
                    logic_expression logicExpr = (logic_expression)query.where_clause.expression;
                    
                    deleteRownumGreaterAndEqual(logicExpr);
                    
                    logicExpr = (logic_expression)query.where_clause.expression;
                    
                    List<expression_element> list = new ArrayList<>();
                    List<expression_element_lt> lts = AstUtil.getDescendantsOfType(logicExpr, expression_element_lt.class);
                    for(expression_element_lt lt : lts) {
                        try {
                            String val = ((general_element_id)((general_element)lt.lhs).general_element_items.get(0)).id.value;
                            if("rownum".equalsIgnoreCase(val)) {
                                list.add(lt);
                                if(node instanceof select_statement) {
                                    ((select_statement) node).setLimitValue(lt);
                                } else if (node instanceof query_block) {
                                    ((query_block) node).setLimitValue(lt);
                                }
                                break;
                            }
                        } catch(Exception e) {   
                        }
                    }
                    
                    List<expression_element_lte> ltes = AstUtil.getDescendantsOfType(logicExpr, expression_element_lte.class);
                    for(expression_element_lte lte : ltes) {
                        try {
                            String val = ((general_element_id)((general_element)lte.lhs).general_element_items.get(0)).id.value;
                            if("rownum".equalsIgnoreCase(val)) {
                                list.add(lte);
                                if(node instanceof select_statement) {
                                    ((select_statement) node).setLimitValue(lte);
                                } else if (node instanceof query_block) {
                                    ((query_block) node).setLimitValue(lte);
                                }
                            }
                        } catch(Exception e) {   
                        }
                    }
                    
                    List<expression_element_eq> eqs = AstUtil.getDescendantsOfType(logicExpr, expression_element_eq.class);
                    for(expression_element_eq eq : eqs) {
                        try {
                            String val = ((general_element_id)((general_element)eq.lhs).general_element_items.get(0)).id.value;
                            if("rownum".equalsIgnoreCase(val)) {
                                list.add(eq);
                                if(node instanceof select_statement) {
                                    ((select_statement) node).setLimitValue(eq);
                                } else if (node instanceof query_block) {
                                    ((query_block) node).setLimitValue(eq);
                                }
                            }
                        } catch(Exception e) {   
                        }
                    }
                    
                    for(expression_element element: list) {
                        _baseNode parent = element._getParent();
                        if(parent.equals(logicExpr)) {
                        	query._replace(query.where_clause, null);
                        	continue;
                        }
                        
                        //get brother node
                        expression_element expression = null;
                        if(parent instanceof expression_element_and){
                            if(((expression_element_and) parent).lhs.equals(element)) {
                                expression = ((expression_element_and) parent).rhs;
                            } else {
                                expression = ((expression_element_and) parent).lhs;
                            }
                        }
                        
                        if(parent instanceof expression_element_or){
                            if(((expression_element_and) parent).lhs.equals(element)) {
                                expression = ((expression_element_and) parent).rhs;
                            } else {
                                expression = ((expression_element_and) parent).lhs;
                            }
                        }
                        
                        if(expression == null) {
                            continue;
                        }
                        
                        //expression_element_and and = new expression_element_and();
                        
                        //delete parent note
                        //put the brother node to parent of parent
                        _baseNode parent2 = element._getParent()._getParent();
                        if(parent2 instanceof expression_element_and) {
                            if(((expression_element_and) parent2).lhs.equals(parent)) {
                                ((expression_element_and) parent2).lhs = expression;
                            } else {
                                ((expression_element_and) parent2).rhs = expression;
                            }
                            //and.lhs = logicExpr.expression_element;
                            //and.rhs = element;
                        } else  if(parent2 instanceof expression_element_or) {
                            if(((expression_element_or) parent2).lhs == parent) {
                                ((expression_element_or) parent2).lhs = expression;
                            } else {
                                ((expression_element_or) parent2).rhs = expression;
                            }
                            
                            //and.lhs = logicExpr.expression_element;
                            //and.rhs = element;
                        } else {
                            //and.lhs = expression;
                            //and.rhs = element;
                        	if(parent2.equals(logicExpr)) {
                        		logicExpr.expression_element = expression;
                            }
                        }
                        //_baseNode children = query.where_clause.expression._getChildren().get(0);
                        //query.where_clause.expression._replace(children, and);
                    }
                }
            } catch(Exception e) {
                //System.out.println(e.getMessage());
            }
    }
    
    public static void changRownumPositionTolast(_baseNode tree){
        List<query_block> query_blocklist = AstUtil.getDescendantsOfType(tree, query_block.class);
        List<select_statement> select_statements = AstUtil.getDescendantsOfType(tree, select_statement.class);
        for(select_statement select : select_statements) {
            try{
                query_block query = (query_block) select.subquery.subquery_basic_elements;
                if(query_blocklist.contains(query)) {
                    query_blocklist.remove(query);
                }
                changeQueryBlockRownum(query, select);
            } catch(Exception e) {
                //System.out.println(e.getMessage());
            }
        }
        
        for(query_block query :query_blocklist){
            changeQueryBlockRownum(query, query);
        }
    }
    
    public static void changeRownumInUpdate(_baseNode tree) {
    	
    	List<update_statement> query_blocklist = AstUtil.getDescendantsOfType(tree, update_statement.class);
    	
    	for(update_statement update : query_blocklist) {
    		try {
	    		if(update.where_clause == null) {
					continue;
				}
	    		
	    		if(getRownums(update.where_clause).size() == 0) {
	    			continue;
	    		}
    		
    			RowNumReturn rnt = changeRownumforUD(update.where_clause, update.general_table_ref.get_dml_table_expression_clause());
    			update.set_subquery_factoring_clause(rnt.getWithclause());
    			update.set_where_clause(rnt.getWhere());
    			
    		} catch(Exception e) {
    			
    		}
    	}
    }
    
    
    public static void changeRownumInDelete(_baseNode tree) {
    	
    	List<delete_statement> query_blocklist = AstUtil.getDescendantsOfType(tree, delete_statement.class);
    	
    	for(delete_statement delete : query_blocklist) {
    		try {
	    		if(delete.where_clause == null) {
					continue;
				}
	    		if(getRownums(delete.where_clause).size() == 0) {
	    			continue;
	    		}
    		
    			RowNumReturn rnt = changeRownumforUD(delete.where_clause, delete.general_table_ref.get_dml_table_expression_clause());
    			delete.set_subquery_factoring_clause(rnt.getWithclause());
    			delete.set_where_clause(rnt.getWhere());
    			
    		} catch(Exception e) {
    			
    		}
    	}
    }
    
    private static List<expression_element> getRownums(where_clause where_clause){
    	logic_expression logicExpr = (logic_expression)where_clause.expression;
        
        deleteRownumGreaterAndEqual(logicExpr);
        
        logicExpr = (logic_expression)where_clause.expression;
        
        List<expression_element> list = new ArrayList<>();
        List<expression_element_lt> lts = AstUtil.getDescendantsOfType(logicExpr, expression_element_lt.class);
        for(expression_element_lt lt : lts) {
            try {
                String val = ((general_element_id)((general_element)lt.lhs).general_element_items.get(0)).id.value;
                if("rownum".equalsIgnoreCase(val)) {
                    list.add(lt);
                    
                }
            } catch(Exception e) {   
            }
        }
        
        List<expression_element_lte> ltes = AstUtil.getDescendantsOfType(logicExpr, expression_element_lte.class);
        for(expression_element_lte lte : ltes) {
            try {
                String val = ((general_element_id)((general_element)lte.lhs).general_element_items.get(0)).id.value;
                if("rownum".equalsIgnoreCase(val)) {
                    list.add(lte);
                }
            } catch(Exception e) {   
            }
        }
        
        List<expression_element_eq> eqs = AstUtil.getDescendantsOfType(logicExpr, expression_element_eq.class);
        for(expression_element_eq eq : eqs) {
            try {
                String val = ((general_element_id)((general_element)eq.lhs).general_element_items.get(0)).id.value;
                if("rownum".equalsIgnoreCase(val)) {
                    list.add(eq);
                }
            } catch(Exception e) {   
            }
        }
        
        return list;
    }
    
    static class RowNumReturn {
    	private subquery_factoring_clause withclause;
    	private where_clause where;
		public subquery_factoring_clause getWithclause() {
			return withclause;
		}
		public void setWithclause(subquery_factoring_clause withclause) {
			this.withclause = withclause;
		}
		public where_clause getWhere() {
			return where;
		}
		public void setWhere(where_clause where) {
			this.where = where;
		}
    }
    
    //M_019 start
    public static void changeMergeInto(_baseNode tree){
    	List<merge_statement> merge_statements = AstUtil.getDescendantsOfType(tree, merge_statement.class);
    	for(merge_statement merge : merge_statements){
    		try {
    			tableview_name tableview_name = merge.tableview_name;
    			alias table_alias = merge.alias;
    			merge_using_clause merge_using_clause = merge.merge_using_clause;
    			merge_update_clause merge_update_clause = merge.merge_update_clause;
    			merge_insert_clause merge_insert_clause = merge.merge_insert_clause;
    			
    			// ALL  or Only Insert
    			int type = 1;
    			//Only Update
    			if(merge_update_clause != null && merge_insert_clause == null){
    				type = 2;
    			}
    			
    			data_manipulation_language_statements dataMani = changeMergeInto(type, tableview_name, table_alias, merge_using_clause, merge_update_clause, merge_insert_clause);
    			if(dataMani != null)
    				merge._getParent()._replace(merge, dataMani);
    			
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.getMessage());
			}
    	}
    }
    
    private static List<expression_element> getColumnsInMergeUsingStatement(expression_element expression){
    	
    	List<expression_element> expressionElements = new ArrayList<expression_element>();
    	
    	if(expression instanceof expression_element_and ){
    		expressionElements.addAll(getColumnsInMergeUsingStatement(((expression_element_and)expression).get_lhs()));
    		expressionElements.addAll(getColumnsInMergeUsingStatement(((expression_element_and)expression).get_rhs()));
    	}
    	else if(expression instanceof expression_element_or){
    		expressionElements.addAll(getColumnsInMergeUsingStatement(((expression_element_or)expression).get_lhs()));
    		expressionElements.addAll(getColumnsInMergeUsingStatement(((expression_element_or)expression).get_rhs()));
    	}
    	else if(expression instanceof expression_element_eq){
    		expressionElements.add(((expression_element_eq)expression).get_lhs());
    		expressionElements.add(((expression_element_eq)expression).get_rhs());
    	}
    	return expressionElements;
    }
    
    private static List<expression_element> filterListWithAlias(List<expression_element> expressions, alias alias){
    	List<expression_element> res = new ArrayList<expression_element>();
    	for(expression_element temp: expressions){
    		if(temp instanceof general_element && alias instanceof table_alias){
    		    general_element_id id = (general_element_id) ((general_element)temp).get_general_element_items().get(0);
    			if(((table_alias)alias).get_id().get_value().equals(id.get_id().get_value())){
        			res.add(temp);
        		}
    		}
    		
    	}
    	return res;
    }
    
    private static List<expression_element> removeAlias(List<expression_element> expressions){
    	for(expression_element temp : expressions){
    		if(temp instanceof general_element){
    			if(((general_element)temp).get_general_element_items().size() > 1){
    				((general_element)temp).get_general_element_items().remove(0);
    			}
    		}
    	}
    	return expressions;
    }
    
    private static data_manipulation_language_statements changeMergeInto(int type, tableview_name _tableview_name, alias _table_alias, merge_using_clause _merge_using_clause, merge_update_clause _merge_update_clause, merge_insert_clause _merge_insert_clause){
    	//Get some Common Properties   	
    	//get aim table view name + alias   table1 a
    	general_element generalElement = new general_element();
    	for(int i = 0; i < _tableview_name.ids.size(); i++ ){
    		general_element_id generalElementId = new general_element_id();
    		generalElementId.set_id(_tableview_name.ids.get(i));
    		generalElement.add_general_element_items(generalElementId);
    	}
    	table_expression tableExpression = new table_expression();
    	tableExpression.set_table_expression_element(generalElement);
    	general_table_ref generalTableRef = new general_table_ref();
    	generalTableRef.set_alias(_table_alias);
    	generalTableRef.set_dml_table_expression_clause(tableExpression);
    	
    	//get aim table view name  table1
    	general_table_ref generalTableRefWithOutAlias = new general_table_ref();
    	generalTableRefWithOutAlias.set_dml_table_expression_clause(tableExpression);
    	
    	//get before table or view names    table2
    	selected_tableview beforeSelectedTableview = _merge_using_clause.get_selected_tableview();
    	select_statement beforeSelectStatement = (select_statement) beforeSelectedTableview.get_selected_tableview_src();
    	subquery beforeSuquery = beforeSelectStatement.get_subquery();
    	query_block beforeQueryBlock = (query_block) beforeSuquery.get_subquery_basic_elements();
    	from_clause beforeFromClause = beforeQueryBlock.get_from_clause();
    	List<table_ref> beforeTableRefAuxList = beforeFromClause.get_table_refs();
    	table_expression beforeTableExpression = new table_expression();
    	table_ref_aux beforeTableRefAux = beforeTableRefAuxList.get(0).get_table_ref_aux();
    	beforeTableExpression = (table_expression) beforeTableRefAux.get_dml_table_expression_clause();
    	
    	//get before alias			b
    	table_alias beforeTableAlias = (table_alias) _merge_using_clause.get_selected_tableview().alias;
    	
    	//get before table view name + alias		table2 b
    	general_table_ref beforeGeneralTableRef = new general_table_ref();
    	beforeGeneralTableRef.set_alias(beforeTableAlias);
    	beforeGeneralTableRef.set_dml_table_expression_clause(beforeTableExpression);
    	
    	//get before columns		col1 col2 col3 col4
    	//not using now
    	/*projection_list beforeProjecttioniList = new projection_list();
    	if(beforeQueryBlock.get_query_block_projection() instanceof projection_list)
    		beforeProjecttioniList = (projection_list) beforeQueryBlock.get_query_block_projection();
    	List<selected_element> beforeSelectedElementList = beforeProjecttioniList.get_selected_elements();*/
    	
    	//get condition    		where a.col1 = b.col1 and a.col2 = b.col2
    	expression  condition = _merge_using_clause.get_expression();
    	where_clause whereClause = new where_clause();
    	whereClause.set_expression(condition);
    	
    	expression_element temp = ((logic_expression)condition).get_expression_element();
    	List<expression_element> allConditionColumns = getColumnsInMergeUsingStatement(temp);
    	//get condition table1's columns
    	List<expression_element> table1Columns = filterListWithAlias(allConditionColumns, _table_alias);
    	List<expression_element> table1ColumnsNoneAlias = new ArrayList<expression_element>();
    			//removeAlias(table1Columns);
    	for(expression_element ee : table1Columns){
    		if(ee instanceof general_element
    				&& ((general_element) ee).get_general_element_items().size() > 1){
    			general_element ge = new general_element();
    			general_element_id gei = new general_element_id();
    			id x = new id();
    			x.set_value(((general_element_id) ((general_element) ee).get_general_element_items().get(1)).get_id().get_value());
    			gei.set_id(x);
    			ge.add_general_element_items(gei); 
    			table1ColumnsNoneAlias.add(ge);
    		}
    	}
    	//get condition table2's columns
    	List<expression_element> table2Columns = filterListWithAlias(allConditionColumns, beforeTableAlias);
    	List<expression_element> table2ColumnsNoneAlias = new ArrayList<expression_element>();
    	for(expression_element ee : table2Columns){
    		if(ee instanceof general_element
    				&& ((general_element) ee).get_general_element_items().size() > 1){
    			general_element ge = new general_element();
    			general_element_id gei = new general_element_id();
    			id x = new id();
    			x.set_value(((general_element_id) ((general_element) ee).get_general_element_items().get(1)).get_id().get_value());
    			gei.set_id(x);
    			ge.add_general_element_items(gei); 
    			table2ColumnsNoneAlias.add(ge);
    		}
    	}
    	
    	//get in_values table ref + alias  in_values as b
    	table_ref tableRefInValue = new table_ref();
    	table_ref_aux tableRefAuxInValue = new table_ref_aux();
    	table_expression tableExpressionInValue = new table_expression();
    	general_element generalElementInValue = new general_element();
    	general_element_id generalElementIdInValue = new general_element_id();
    	id id1 = new id();
    	id1.set_value("in_values");
    	generalElementIdInValue.set_id(id1);
    	generalElementInValue.add_general_element_items(generalElementIdInValue);
    	tableExpressionInValue.set_table_expression_element(generalElementInValue);
    	tableRefAuxInValue.set_dml_table_expression_clause(tableExpressionInValue);
    	tableRefAuxInValue.set_alias(beforeTableAlias);
    	tableRefInValue.set_table_ref_aux(tableRefAuxInValue);
    	
    	//get in_values as a
    	table_ref tableRefInValueAimAlias = new table_ref();
    	table_ref_aux tableRefAuxInValueAimAlias = new table_ref_aux();
    	tableRefAuxInValueAimAlias.set_dml_table_expression_clause(tableExpressionInValue);
    	tableRefAuxInValueAimAlias.set_alias(_table_alias);
    	tableRefInValueAimAlias.set_table_ref_aux(tableRefAuxInValueAimAlias);
    	
//package 
    	//Head
    	//Common: in_values as 
		factoring_element factoringElement1 = new factoring_element();
		query_name queryName1 = new query_name();
    	queryName1.set_id(id1);
		factoringElement1.set_query_name(queryName1);	
    	
		subquery subqueryInValues = new subquery();
		query_block queryBlockInvalues = new query_block();
		//select * 
		projection_asterisk projectionAsterisk1 = new projection_asterisk();
		queryBlockInvalues.set_query_block_projection(projectionAsterisk1);
		
		//from table1
		from_clause fromClauseInValue1 = new from_clause();
		table_ref tableRefInValue1 = new table_ref();
		table_ref_aux tableRefAuxInValue1 = new table_ref_aux();
		tableRefAuxInValue1.set_dml_table_expression_clause(tableExpression);
		tableRefInValue1.set_table_ref_aux(tableRefAuxInValue1);
		fromClauseInValue1.add_table_refs(tableRefInValue1);
		queryBlockInvalues.set_from_clause(fromClauseInValue1);
		//where (col1,col2) in (select col1,col2 from table2))
		where_clause whereClauseInValue1 = new where_clause();
		general_expression generalExpression1 = new general_expression();
		expression_element_in expressionElementIn = new expression_element_in();
		//(col1,col2)
		general_element_vector generalElementVector = new general_element_vector();
		for(expression_element ee : table1ColumnsNoneAlias){
			general_expression ge = new general_expression();
			ge.set_expression_element(ee);
			generalElementVector.add_general_expressions(ge);
		}
		expressionElementIn.set_arg(generalElementVector);
		//(select col1,col2 from table2)
		subquery subqueryInValue2 = new subquery();
		query_block queryBlock2 = new query_block();
			//select col1,col2
			projection_list projectionList2 = new projection_list();
			for(expression_element ee: table2ColumnsNoneAlias){
				selected_element se = new selected_element();
				general_expression ge = new general_expression();
				ge.set_expression_element(ee);
				se.set_expression(ge);
				projectionList2.add_selected_elements(se);
			}
			queryBlock2.set_query_block_projection(projectionList2);
			//from table2
			from_clause fromClause2 = new from_clause();
			table_ref tableRef2 = new table_ref();
			table_ref_aux tableRefAux2 = new table_ref_aux();
			tableRefAux2.set_dml_table_expression_clause(beforeTableExpression);
			tableRef2.set_table_ref_aux(tableRefAux2);
			fromClause2.add_table_refs(tableRef2);
			queryBlock2.set_from_clause(fromClause2);
		subqueryInValue2.set_subquery_basic_elements(queryBlock2);
			
		expressionElementIn.set_in_elements(subqueryInValue2);
		generalExpression1.set_expression_element(expressionElementIn);
		whereClauseInValue1.set_expression(generalExpression1);
		queryBlockInvalues.set_where_clause(whereClauseInValue1);
		subqueryInValues.set_subquery_basic_elements(queryBlockInvalues);
		
		factoringElement1.set_subquery(subqueryInValues);
    	
    	

    	switch (type) {
		case 1:
			insert_statement insertStatement = new insert_statement();
	    	single_table_insert singleTableInsert = new single_table_insert();
	    	
	    	//with in_values as (select * from table1 where (col1,col2) in (select col1,col2 from table2))
	    	
	    	//with Head
	    	subquery_factoring_clause subquertFactoringClause = new subquery_factoring_clause();
			subquertFactoringClause.add_factoring_elements(factoringElement1);
			
			//upsert as (update table1 as a set col3= b.col3,col4 = b.col4 from in_values b where a.col1 = b.col1 and a.col2 = b.col2)
			if(_merge_update_clause != null){
				factoring_element factoringElement2 = new factoring_element();
				
				//get upsert table ref
		    	table_ref tableRefUpsert = new table_ref();
		    	table_ref_aux tableRefAuxUpsert = new table_ref_aux();
		    	table_expression tableExpressionUpsert = new table_expression();
		    	general_element generalElementUpsert = new general_element();
		    	general_element_id generalElementIdUpsert = new general_element_id();
		    	id id2 = new id();
		    	id2.set_value("upsert");
		    	generalElementIdUpsert.set_id(id2);
		    	generalElementUpsert.add_general_element_items(generalElementIdUpsert);
		    	tableExpressionUpsert.set_table_expression_element(generalElementUpsert);
		    	tableRefAuxUpsert.set_dml_table_expression_clause(tableExpressionUpsert);
		    	tableRefUpsert.set_table_ref_aux(tableRefAuxUpsert);
		    	//get merge updateclause (a.col3 = b.col3, a.col4 = b.col4)
		    	List<merge_element> mergeElements = _merge_update_clause.get_merge_elements();
		    	
		    	//upsert as
		    	query_name queryName2 = new query_name();
		    	queryName2.set_id(id2);
		    	factoringElement2.set_query_name(queryName2);
		    	//update table1 as a set col3= b.col3,col4 = b.col4 from in_values b where a.col1 = b.col1 and a.col2 = b.col2
		    	update_statement updateStatement2 = new update_statement();
		    	//update table1 a
		    	updateStatement2.set_general_table_ref(generalTableRef);
		    	//set col3 = b.col3,col4 = b.col4
		    	update_statement_set updateStetementSet2 = new update_statement_set();
		    	for(merge_element me : mergeElements){
		    		update_set_elements_assign updateSetElementsAssign2 = new update_set_elements_assign();
			    	updateSetElementsAssign2.add_column_names(me.get_column_name());
		    		updateSetElementsAssign2.set_expression_or_subquery(me.get_expression());
		    		updateStetementSet2.add_update_set_elementss(updateSetElementsAssign2);
		    	}
		    	updateStatement2.set_update_statement_set(updateStetementSet2);
		    	//from in_values b
		    	from_clause fromClause2InUpdate = new from_clause();
		    	fromClause2InUpdate.add_table_refs(tableRefInValue);
		    	updateStatement2.set_from_clause(fromClause2InUpdate);
		    	//where a.col1 = b.col1 and a.col2 = b.col2
		    	if(_merge_update_clause.get_where_clause() != null){
		    		where_clause wc = new where_clause();
		    		logic_expression le = new logic_expression();
		    		expression_element_and eea = new expression_element_and();
		    		eea.set_lhs(((logic_expression)whereClause.get_expression()).get_expression_element());
		    		eea.set_rhs(((logic_expression)_merge_update_clause.get_where_clause().get_expression()).get_expression_element());
		    		le.set_expression_element(eea);
		    		wc.set_expression(le);
		    		updateStatement2.set_where_clause(wc);
		    	}else
		    		updateStatement2.set_where_clause(whereClause);
		    	
		    	factoringElement2.set_update_statement(updateStatement2);
		    	subquertFactoringClause.add_factoring_elements(factoringElement2);
			}
	    	
	    	singleTableInsert.set_subquery_factoring_clause(subquertFactoringClause);
	    	
			if(_merge_insert_clause != null){
				//insert into table1(col1,col2,col3,col4) select (col1,col2,col3,col4) from table2 b where not exists(select 1 from in_values a where a.col1 = b.col1 and a.col2 = b.col2)
				
				//get merge insert clause 's aim columns :(a.col1,a,col2,a.col3,a.col4)
				List<column_name> getInsertAimColumns = _merge_insert_clause.get_insert_into_clause_columns().get_column_names();
				for(column_name cn : getInsertAimColumns){
					if(cn.get_ids().size() > 1){
						//remove alias
						cn.get_ids().remove(0);
					}
				}
				//get merge insert clause 's old columns :(b.col1,b.col2,b.col3,b.col4)
				List<expression> getInsertOldColumns = _merge_insert_clause.get_expression_list().get_expressions();
				List<expression_element> getInsertOldColumnsNoneAlias = new ArrayList<expression_element>();
				for(expression e : getInsertOldColumns){
					if(e instanceof general_expression){
						getInsertOldColumnsNoneAlias.add(((general_expression) e).get_expression_element());
					}
				}
				if(getInsertOldColumnsNoneAlias.size() > 0)
					removeAlias(getInsertOldColumnsNoneAlias);
				
				//insert into table1(col1,col2,col3,col4)
				insert_into_clause insertIntoClause3 = new insert_into_clause();
				insertIntoClause3.set_general_table_ref(generalTableRefWithOutAlias);
				insert_into_clause_columns insertIntoClauseColumns3 = new insert_into_clause_columns();
				insertIntoClauseColumns3.column_names = getInsertAimColumns;
				insertIntoClause3.set_insert_into_clause_columns(insertIntoClauseColumns3);
				
				singleTableInsert.set_insert_into_clause(insertIntoClause3);
				//select (col1,col2,col3,col4) from table2 b where not exists(select 1 from in_values a where a.col1 = b.col1 and a.col2 = b.col2)
				select_statement selectStatement = new select_statement();
				subquery subquery3InInsert = new subquery();
				query_block queryBlock3InInsert= new query_block();
				
					//select (col1,col2,col3,col4)
					projection_list projectionList3OldExpressions = new projection_list();
					for(expression ge : getInsertOldColumns){
						if(ge instanceof general_expression){
							selected_element se = new selected_element();
							se.set_expression(ge);
							projectionList3OldExpressions.add_selected_elements(se);
						}
					}
					
					queryBlock3InInsert.set_query_block_projection(projectionList3OldExpressions);
					//from table2 b
					from_clause fromClause3InInsert = new from_clause();
					table_ref tableRef3InInsert = new table_ref();
					table_ref_aux tableRefAux3InInsert = new table_ref_aux();
					tableRefAux3InInsert.set_alias(beforeTableAlias);
					tableRefAux3InInsert.set_dml_table_expression_clause(beforeTableExpression);
					tableRef3InInsert.set_table_ref_aux(tableRefAux3InInsert);
					fromClause3InInsert.add_table_refs(tableRef3InInsert);
					
					queryBlock3InInsert.set_from_clause(fromClause3InInsert);
					//where not exists(select 1 from in_values a where a.col1 = b.col1 and a.col2 = b.col2)
					where_clause whereClause3InInsert = new where_clause();
					logic_expression logicExpression3InInsert = new logic_expression();
					expression_element_not expressionElementNot3 = new expression_element_not();
					expression_element_exists expressionElementExists3 = new expression_element_exists();
						//select 1 from in_values a where a.col1 = b.col1 and a.col2 = b.col2
						subquery subquery3InSelect = new subquery();
						query_block queryBlock3InSelect = new query_block();
						//select 1
						projection_list projectionList3InSelect = new projection_list();
						selected_element selectElement3InSelect = new selected_element();
						general_expression generalExpression3InSelect = new general_expression();
						general_element generalElement3InSelect = new general_element();
						general_element_id gElementId3InSelect = new general_element_id();
						id id3InSelect = new id();
						id3InSelect.set_value("1");
						gElementId3InSelect.set_id(id3InSelect);
						generalElement3InSelect.add_general_element_items(gElementId3InSelect);
						generalExpression3InSelect.set_expression_element(generalElement3InSelect);
						selectElement3InSelect.set_expression(generalExpression3InSelect);
						projectionList3InSelect.add_selected_elements(selectElement3InSelect);
						
						queryBlock3InSelect.set_query_block_projection(projectionList3InSelect);
						//from in_values a
						from_clause fromClause3InSelect = new from_clause();
						fromClause3InSelect.add_table_refs(tableRefInValueAimAlias);
						
						queryBlock3InSelect.set_from_clause(fromClause3InSelect);
						//where ...
						queryBlock3InSelect.set_where_clause(whereClause);
						
						subquery3InSelect.set_subquery_basic_elements(queryBlock3InSelect);
				
					expressionElementExists3.set_expression_or_subquery(subquery3InSelect);
					expressionElementNot3.set_arg(expressionElementExists3);
					logicExpression3InInsert.set_expression_element(expressionElementNot3);
					whereClause3InInsert.set_expression(logicExpression3InInsert);
				
					if(_merge_insert_clause.get_where_clause() != null){
			    		where_clause wc = new where_clause();
			    		logic_expression le = new logic_expression();
			    		expression_element_and eea = new expression_element_and();
			    		eea.set_lhs(((logic_expression)whereClause3InInsert.get_expression()).get_expression_element());
			    		eea.set_rhs(((logic_expression)_merge_insert_clause.get_where_clause().get_expression()).get_expression_element());
			    		le.set_expression_element(eea);
			    		wc.set_expression(le);
			    		queryBlock3InInsert.set_where_clause(wc);
			    	}else
			    		queryBlock3InInsert.set_where_clause(whereClause3InInsert);
					
				subquery3InInsert.set_subquery_basic_elements(queryBlock3InInsert);
				selectStatement.set_subquery(subquery3InInsert);
				singleTableInsert.set_select_statement(selectStatement);
			}
	    	insertStatement.set_insert_statement_spec(singleTableInsert);
	    	return insertStatement;

		case 2:
			
			update_statement onlyUpdateUpdateStatement2 = new update_statement();
	    	
	    	//with in_values as (select * from table1 where (col1,col2) in (select col1,col2 from table2))
	    	
	    	//with Head
	    	subquery_factoring_clause onlyUpdateSubquertFactoringClause = new subquery_factoring_clause();
			onlyUpdateSubquertFactoringClause.add_factoring_elements(factoringElement1);
			
	    	onlyUpdateUpdateStatement2.set_subquery_factoring_clause(onlyUpdateSubquertFactoringClause);
			//update table1 as a set col3= b.col3,col4 = b.col4 from in_values b where a.col1 = b.col1 and a.col2 = b.col2
	    	
	    	//get merge updateclause (a.col3 = b.col3, a.col4 = b.col4)
	    	List<merge_element> onlyUpdateMergeElements = _merge_update_clause.get_merge_elements();
	    	
	    	//update table1 a
	    	onlyUpdateUpdateStatement2.set_general_table_ref(generalTableRef);
	    	//set col3 = b.col3,col4 = b.col4
	    	update_statement_set onlyUpdateUpdateStetementSet2 = new update_statement_set();
	    	for(merge_element me : onlyUpdateMergeElements){
	    		update_set_elements_assign updateSetElementsAssign2 = new update_set_elements_assign();
		    	updateSetElementsAssign2.add_column_names(me.get_column_name());
	    		updateSetElementsAssign2.set_expression_or_subquery(me.get_expression());
	    		onlyUpdateUpdateStetementSet2.add_update_set_elementss(updateSetElementsAssign2);
	    	}
	    	onlyUpdateUpdateStatement2.set_update_statement_set(onlyUpdateUpdateStetementSet2);
	    	//from in_values b
	    	from_clause onlyUpdateFromClause2InUpdate = new from_clause();
	    	onlyUpdateFromClause2InUpdate.add_table_refs(tableRefInValue);
	    	onlyUpdateUpdateStatement2.set_from_clause(onlyUpdateFromClause2InUpdate);
	    	//where a.col1 = b.col1 and a.col2 = b.col2
	    	if(_merge_update_clause.get_where_clause() != null){
	    		where_clause wc = new where_clause();
	    		logic_expression le = new logic_expression();
	    		expression_element_and eea = new expression_element_and();
	    		eea.set_lhs(((logic_expression)whereClause.get_expression()).get_expression_element());
	    		eea.set_rhs(((logic_expression)_merge_update_clause.get_where_clause().get_expression()).get_expression_element());
	    		le.set_expression_element(eea);
	    		wc.set_expression(le);
	    		onlyUpdateUpdateStatement2.set_where_clause(wc);
	    	}else
	    		onlyUpdateUpdateStatement2.set_where_clause(whereClause);
	    		
	    	return onlyUpdateUpdateStatement2;
		}
    	return null;
    	
    }
    
    //fenglu add end
    private static RowNumReturn changeRownumforUD(where_clause where_clause, dml_table_expression_clause tableExp) {
    	logic_expression where = (logic_expression)where_clause.expression;
		
		
		query_block querytemp = new query_block();
		
		//from
		from_clause from = new from_clause();
		table_ref_aux tablerefaux = new table_ref_aux();
		tablerefaux.set_dml_table_expression_clause(tableExp);
		table_ref table = new table_ref();
		table.set_table_ref_aux(tablerefaux);
		from.add_table_refs(table);
		querytemp.set_from_clause(from);
		
		//where 
		querytemp.set_where_clause(where_clause);
		
		//select element
		projection_list pl = new projection_list();
		selected_element se = new selected_element();
		general_expression ge = new general_expression();
		general_element gElement = new general_element();
		general_element_id gElementId = new general_element_id();
		id id = new id();
		id.set_value("ctid");
		gElementId.set_id(id);
		gElement.add_general_element_items(gElementId);
		ge.set_expression_element(gElement);
		se.set_expression(ge);
		pl.add_selected_elements(se);
		
		querytemp.set_query_block_projection(pl);
		
		
		//add with clause before update
		subquery_factoring_clause withclause = new subquery_factoring_clause();
		factoring_element factoring = new factoring_element();
		subquery subquery = new subquery();
		subquery.set_subquery_basic_elements(querytemp);
		factoring.set_subquery(subquery);
		query_name queryName = new query_name();
		id id2 = new id();
		id2.set_value("cte");
		queryName.set_id(id2);
		factoring.set_query_name(queryName);
		withclause.add_factoring_elements(factoring);
		//update.set_subquery_factoring_clause(withclause);
		
		//set new where_clause to update
		where_clause newwhere = new where_clause();
		logic_expression newlogicExp = new logic_expression();
		
		expression_element_in in = new expression_element_in();
		
		subquery inSubquery = new subquery();
		query_block inqueryblock = new query_block();
		
		//add from at in_clause in where_clause of update
		from_clause infrom = new from_clause();
		table_ref inTable_ref = new table_ref();
		table_ref_aux intablerefaux = new table_ref_aux();
		table_expression intableexpression = new table_expression();
		general_element inGeneral_element = new general_element();
		general_element_id inGeneral_element_id = new general_element_id();
		id intableName = new id();
		intableName.set_value("cte");
		inGeneral_element_id.set_id(intableName);
		inGeneral_element.add_general_element_items(inGeneral_element_id);
		intableexpression.set_table_expression_element(inGeneral_element);
		intablerefaux.set_dml_table_expression_clause(intableexpression);
		inTable_ref.set_table_ref_aux(intablerefaux);
		infrom.add_table_refs(inTable_ref);
		inqueryblock.set_from_clause(infrom);
		
		
//		projection_list pl = new projection_list();
//		selected_element se = new selected_element();
//		general_expression ge = new general_expression();
//		general_element gElement = new general_element();
//		general_element_id gElementId = new general_element_id();
//		id id = new id();
//		id.set_value("ctid");
//		gElementId.set_id(id);
//		gElement.add_general_element_items(gElementId);
//		ge.set_expression_element(gElement);
//		se.set_expression(ge);
//		pl.add_selected_elements(se);
		
		inqueryblock.set_query_block_projection(pl);
		

		inSubquery.set_subquery_basic_elements(inqueryblock);
		
		
		in.set_in_elements(inSubquery);
		
		general_element inarg = new general_element();
		general_element_id inargelementid = new general_element_id();
		id inargid = new id();
		inargid.set_value("ctid");
		inargelementid.set_id(inargid);
		inarg.add_general_element_items(inargelementid);
		in.set_arg(inarg);
		
		newlogicExp.set_expression_element(in);
		
		newwhere.set_expression(newlogicExp);
		
		//where_clause = newwhere;
		
		RowNumReturn ret = new RowNumReturn();
		ret.setWhere(newwhere);
		ret.setWithclause(withclause);
		
		return ret;
    }
    
    public static void handlerIsNUll(_baseNode tree) {
    	List<expression_element_null> elementlist = AstUtil.getDescendantsOfType(tree, expression_element_null.class);
    	for(expression_element_null element : elementlist) {
    		_baseNode parent = element._getParent();
    		expression_element_or or = new expression_element_or();
    		
    		expression_element_eq eq = new expression_element_eq();
    		eq.lhs = element.get_arg();
    		constant_char_string empty = new constant_char_string();
    		empty.set_value("''");
    		eq.rhs = empty;
    		
    		or.lhs = element;
    		or.rhs = eq;
    		
    		parent._replace(element, or);
    	}
    }
    
    /**
     * check the connect clause is exist in query
     * @param query
     * @return true:exist false:not exist
     * */
    private static List<query_block> getConnectSubQuery(query_block query, List<query_block> querys) {
    	
    	List<query_block> queryList = new ArrayList<>();
    	querys.remove(query);
    	if(query.get_hierarchical_query_clause() != null) {
    		queryList.add(query);
    	}
    	
    	List<query_block> subquerys = AstUtil.getDescendantsOfType(query.get_from_clause(), query_block.class);
    	for(query_block q : subquerys) {
    		querys.remove(q);
    		if(q.get_hierarchical_query_clause() != null) {
    			queryList.add(q);
        	}
    	}

    	return queryList;
    }
    
    public static void handlerConnectBy2(_baseNode tree) {
    	List<query_block> querys = AstUtil.getDescendantsOfType(tree, query_block.class);
    	
    	while(querys.size() > 0) {
    		try {
	    		query_block query = querys.get(0);
	    		List<query_block> subQuerys = getConnectSubQuery(query, querys);
	    		
	    		if(subQuerys.size() == 0) {
	    			continue;
	    		}
	    		
	    		// parent is subquery, parent of parent is select_statement
	    		if(!(query._getParent()._getParent() instanceof select_statement)) {
	    			continue;
	    		}
	    		
	    		
	    		
	    		subquery_factoring_clause withclause = new subquery_factoring_clause();
	    		for(query_block subquery: subQuerys) {
	    			handlerConnectBySub2(subquery, withclause);
	    		}
	    		
	    		select_statement select = (select_statement) query._getParent()._getParent();
	    		select.set_subquery_factoring_clause(withclause);
    		} catch(Exception e) {
    			System.out.println("---------------" + e.getMessage());
    		}
    	}
    	
    }
    
    private static void handlerConnectBySub2(query_block query, subquery_factoring_clause withclause) {
    	//connection clause
		hierarchical_query_clause connectionClause = query.get_hierarchical_query_clause();
		
		//get expression from connection clause 
		logic_expression connectExp = (logic_expression) connectionClause.get_hierarchical_query_clause_connect().get_expression();
		//get PRIOR clause from connection expression
		expression_element_eq connectionExpEq = (expression_element_eq)connectExp.get_expression_element();
		general_element_item priorExp = null;
		general_element_item notPriorExp = null;
		
		if(connectionExpEq.lhs instanceof expression_element_prior) {
			List<general_element_item> listitems = (List<general_element_item>)ReflectionUtil.callMethod(ReflectionUtil.callMethod(connectionExpEq.lhs, "get_arg"), "get_general_element_items");
 			priorExp = listitems.get(listitems.size() - 1);
 			listitems = (List<general_element_item>)ReflectionUtil.callMethod(connectionExpEq.rhs,  "get_general_element_items");
 			notPriorExp = listitems.get(listitems.size() - 1); 
		} else if(connectionExpEq.rhs instanceof expression_element_prior) {
			List<general_element_item> listitems = (List<general_element_item>)ReflectionUtil.callMethod(ReflectionUtil.callMethod(connectionExpEq.rhs, "get_arg"), "get_general_element_items");
 			priorExp = listitems.get(listitems.size() - 1);
 			listitems = (List<general_element_item>)ReflectionUtil.callMethod(connectionExpEq.lhs,  "get_general_element_items");
 			notPriorExp = listitems.get(listitems.size() - 1); 
		} else {
			List<general_element_item> listitems =  (List<general_element_item>)ReflectionUtil.callMethod(connectionExpEq.rhs, "get_general_element_items");
 			priorExp = listitems.get(listitems.size() - 1);
 			listitems = (List<general_element_item>)ReflectionUtil.callMethod(connectionExpEq.lhs,  "get_general_element_items");
 			notPriorExp = listitems.get(listitems.size() - 1); 
		}
		
		//from clause
		from_clause fromClause = query.get_from_clause();
		
		//start clause
		start_part startClause = connectionClause.get_start_part();
		
		/* make the query in with clause start */
		factoring_element factoringElement = new factoring_element();
		//query name in with clause
		String strQueryName = "cte_" + (withclause.get_factoring_elements().size());
		query_name queryName = new query_name();
		id queryNameId = new id();
		queryNameId.set_value(strQueryName);
		queryName.set_id(queryNameId);
		factoringElement.set_query_name(queryName);
		
		subquery subquery = new subquery();
		query_block queryBlock = new query_block();
		//from clause
		queryBlock.set_from_clause(fromClause);
		//select list -> *
		projection_asterisk asterisk = new projection_asterisk();
		queryBlock.set_query_block_projection(asterisk);
		if(startClause != null) {
			where_clause whereClause = new where_clause();
			whereClause.set_expression(startClause.get_expression());
			queryBlock.set_where_clause(whereClause);
		}
		subquery.set_subquery_basic_elements(queryBlock);
		//union in query
		subquery_operation_union union = new subquery_operation_union();
		
		//union all
		CommonToken t = new CommonToken(PLSQLPrinter.SQL92_RESERVED_ALL);
		t.setText("ALL");
		CommonTree commonTree = new CommonTree(t);
		union.set_SQL92_RESERVED_ALL(commonTree);
		
		query_block unionBlock = new query_block();
		
		//make from clause in union query 
		from_clause unionFrom = new from_clause();
		//TODO
		table_ref tableRef = fromClause.get_table_refs().get(0);
		table_ref_aux tableRefAux = tableRef.get_table_ref_aux();
		dml_table_expression_clause tableexpressionclause = tableRefAux.get_dml_table_expression_clause();
		alias kalias = tableRefAux.get_alias();
		if(kalias == null) {
			// If alias is not exist, 'k' is setted 
			kalias = new table_alias();
			id id = new id();
			id.set_value("k");
			((table_alias)kalias).set_id(id);
		}
		tableRef = new table_ref();
		tableRefAux = new table_ref_aux();
		tableRefAux.set_dml_table_expression_clause(tableexpressionclause);
		tableRefAux.set_alias(kalias);
		tableRef.set_table_ref_aux(tableRefAux);
		unionFrom.add_table_refs(tableRef);
		//table cte
		tableRef = new table_ref();
		tableRefAux = new table_ref_aux();
		table_alias cteAlias = new table_alias();
		id cid = new id();
		cid.set_value("c");
		cteAlias.set_id(cid);
		tableRefAux.set_alias(cteAlias);
		table_expression tableExp = new table_expression();
		general_element generalElement = new general_element();
		general_element_id generalElementId = new general_element_id();
		generalElementId.set_id(queryNameId);
		generalElement.add_general_element_items(generalElementId);
		tableExp.set_table_expression_element(generalElement);
		tableRefAux.set_dml_table_expression_clause(tableExp);
		tableRef.set_table_ref_aux(tableRefAux);
		unionFrom.add_table_refs(tableRef);
		unionBlock.set_from_clause(unionFrom);
		//make where in union clause
		where_clause unionWhere = new where_clause();
		expression_element_eq expEq = new expression_element_eq();
		generalElement = new general_element();
		generalElementId = new general_element_id();
		generalElementId.set_id(cid);
		generalElement.add_general_element_items(generalElementId);
		generalElement.add_general_element_items(priorExp);
		expEq.set_lhs(generalElement);
		generalElement = new general_element();
		generalElementId = new general_element_id();
		generalElementId.set_id(((table_alias)kalias).get_id());
		generalElement.add_general_element_items(generalElementId);
		generalElement.add_general_element_items(notPriorExp);
		expEq.set_rhs(generalElement);
		logic_expression logicExp = new logic_expression();
		logicExp.set_expression_element(expEq);
		unionWhere.set_expression(logicExp);
		unionBlock.set_where_clause(unionWhere);
		
		//select list
		projection_list unionSelect = new projection_list();
		selected_element selectElement = new selected_element();
		expression_element_dot_asterisk dotasterisk = new expression_element_dot_asterisk();
		tableview_name tableName = new tableview_name();
		tableName.add_ids(((table_alias)kalias).get_id());
		dotasterisk.set_tableview_name(tableName);
		general_expression generalExp = new general_expression();
		generalExp.set_expression_element(dotasterisk);
		selectElement.set_expression(generalExp);
		unionSelect.add_selected_elements(selectElement);
		unionBlock.set_query_block_projection(unionSelect);

		union.set_subquery_basic_elements(unionBlock);
		subquery.add_subquery_operation_parts(union);
		
		factoringElement.set_subquery(subquery);

		//update from clause for query 
		from_clause newfrom = new from_clause();
		tableRef = new table_ref();
		tableRefAux = new table_ref_aux();
		tableExp = new table_expression();
		generalElement = new general_element();
		generalElementId = new general_element_id();
		generalElementId.set_id(withclause.get_factoring_elements().get(withclause.get_factoring_elements().size() -1).get_query_name().get_id());
		generalElement.add_general_element_items(generalElementId);
		tableExp.set_table_expression_element(generalElement);
		tableRefAux.set_dml_table_expression_clause(tableExp);
		tableRef.set_table_ref_aux(tableRefAux);
		newfrom.add_table_refs(tableRef);
		query.set_from_clause(newfrom);
    }
    
    public static void handlerConnectBy(_baseNode tree) {
    	List<select_statement> select_statements = AstUtil.getDescendantsOfType(tree, select_statement.class);
    	for(select_statement select: select_statements) {
    		try {
    			select.get_subquery().get_subquery_basic_elements();
    			
    			subquery_factoring_clause withclause = new subquery_factoring_clause();

    			subquery subquery = select.get_subquery();
    			
    			query_block query = (query_block) subquery.get_subquery_basic_elements();
    			
    			if(handlerConnectBySub(query, withclause)) {
    				//recursive
        			CommonToken t = new CommonToken(PLSQLPrinter.RECURSIVE_VK);
        			t.setText("recursive");
        			CommonTree commonTree = new CommonTree(t);
        			withclause.set_RECURSIVE_VK(commonTree);
    				
    				select.set_subquery_factoring_clause(withclause);
    				
    				//update from clause for query 
    				from_clause from = new from_clause();
    				table_ref tableRef = new table_ref();
    				table_ref_aux tableRefAux = new table_ref_aux();
    				table_expression tableExp = new table_expression();
    				general_element generalElement = new general_element();
    				general_element_id generalElementId = new general_element_id();
    				generalElementId.set_id(withclause.get_factoring_elements().get(withclause.get_factoring_elements().size() -1).get_query_name().get_id());
    				generalElement.add_general_element_items(generalElementId);
    				tableExp.set_table_expression_element(generalElement);
    				tableRefAux.set_dml_table_expression_clause(tableExp);
    				tableRef.set_table_ref_aux(tableRefAux);
    				from.add_table_refs(tableRef);
    				query.set_from_clause(from);
    				
    				query_block_projection selectList = query.get_query_block_projection();
    				if(selectList instanceof projection_list) {
    					projection_list list = (projection_list)selectList;
    					for(selected_element selectElement : list.get_selected_elements()) {
    						//delete the table name of coloum in selectItem
    						expression expression = selectElement.get_expression();
    						if(expression instanceof general_expression) {
    							expression_element element = ((general_expression) expression).get_expression_element();
    							if(element instanceof general_element) {
    								List<general_element_item> items = ((general_element) element).get_general_element_items();
    								if(items.size() == 2) {
    									items.remove(0);
    								}
    							}
    						}
    					}
    				}
    				//delete connection by
    				query.set_hierarchical_query_clause(null);
    			}
    			
    			//handler union parts
    			List<subquery_operation_part> parts = subquery.get_subquery_operation_parts();
    			for(subquery_operation_part part : parts) {
    				if(part instanceof subquery_operation_union) {
    					query = (query_block) ((subquery_operation_union) part).get_subquery_basic_elements();
    					
    				}
    			}

    		} catch(Exception e) {
    			System.out.println("---------------" + e.getMessage());
    		}
    	}
    }
    
    /**
     * handler table ref aux :connect by -> with clause
     * @param table ref aux
     * @param with clause
     * */
    private static void handlerTableRefAuxInFrom(table_ref_aux tableRefAux, subquery_factoring_clause withclause) throws Exception {
    	
    	List<select_statement> select_statements = AstUtil.getDescendantsOfType(tableRefAux, select_statement.class);
    	
    	for(int i = select_statements.size() - 1 ; i >= 0; i-- ) {
    		select_statement select = select_statements.get(i);
    		query_block query = (query_block) select.get_subquery().get_subquery_basic_elements();
    		if(handlerConnectBySub(query, withclause)) {
				//connect by clause is exist in sub select 
				factoring_element withElement = withclause.get_factoring_elements().get(withclause.get_factoring_elements().size() - 1);
				general_element generalElement = new general_element();
				general_element_id generalElementId = new general_element_id();
				generalElementId.set_id(withElement.get_query_name().get_id());
				generalElement.add_general_element_items(generalElementId);
				
				select._getParent()._getParent()._replace(select._getParent(), generalElement);
			} else {
				//connect clause is not exist in sub select
				factoring_element factoringElement = new factoring_element();
				factoringElement.set_subquery(select.get_subquery());
				query_name queryName = new query_name();
				id queryNameId = new id();
				queryNameId.set_value("sub_" + withclause.get_factoring_elements().size());
				queryName.set_id(queryNameId);
				factoringElement.set_query_name(queryName);
				withclause.add_factoring_elements(factoringElement);
				
				//update from clause
				general_element generalElement = new general_element();
				general_element_id generalElementId = new general_element_id();
				generalElementId.set_id(queryNameId);
				generalElement.add_general_element_items(generalElementId);
				_baseNode node = select._getParent();
				node._getParent()._replace(node, generalElement);
			}
    	}
    }

    /**
     * handler connect by 
     * @param select statement
     * @param with clause
     * @return ture: connect clause is exist. false: is mot exist
     * @throws Exception 
     * */
	private static boolean handlerConnectBySub(query_block query, subquery_factoring_clause withclause) throws Exception {
		//query_block query = (query_block) select.get_subquery().get_subquery_basic_elements();
		
		//If connection clause is not exist, return
		if(query.get_hierarchical_query_clause() == null) {
			return false;
		}
		
		//connection clause
		hierarchical_query_clause connectionClause = query.get_hierarchical_query_clause();
		
		//get expression from connection clause 
		logic_expression connectExp = (logic_expression) connectionClause.get_hierarchical_query_clause_connect().get_expression();
		//get PRIOR clause from connection expression
		expression_element_eq connectionExpEq = (expression_element_eq)connectExp.get_expression_element();
		general_element_item priorExp = null;
		general_element_item notPriorExp = null;
		
		if(connectionExpEq.lhs instanceof expression_element_prior) {
			List<general_element_item> listitems = (List<general_element_item>)ReflectionUtil.callMethod(ReflectionUtil.callMethod(connectionExpEq.lhs, "get_arg"), "get_general_element_items");
 			priorExp = listitems.get(listitems.size() - 1);
 			listitems = (List<general_element_item>)ReflectionUtil.callMethod(connectionExpEq.rhs,  "get_general_element_items");
 			notPriorExp = listitems.get(listitems.size() - 1); 
		} else if(connectionExpEq.rhs instanceof expression_element_prior) {
			List<general_element_item> listitems = (List<general_element_item>)ReflectionUtil.callMethod(ReflectionUtil.callMethod(connectionExpEq.rhs, "get_arg"), "get_general_element_items");
 			priorExp = listitems.get(listitems.size() - 1);
 			listitems = (List<general_element_item>)ReflectionUtil.callMethod(connectionExpEq.lhs,  "get_general_element_items");
 			notPriorExp = listitems.get(listitems.size() - 1); 
		} else {
			List<general_element_item> listitems =  (List<general_element_item>)ReflectionUtil.callMethod(connectionExpEq.rhs, "get_general_element_items");
 			priorExp = listitems.get(listitems.size() - 1);
 			listitems = (List<general_element_item>)ReflectionUtil.callMethod(connectionExpEq.lhs,  "get_general_element_items");
 			notPriorExp = listitems.get(listitems.size() - 1); 
		}
		
		//from in query
		from_clause fromClause = query.get_from_clause();
		//loop the table ref in from claus
		//List<table_expression_element> tableElementList = new ArrayList<>();
		List<table_ref_aux> tableElementList = new ArrayList<>();
		for(table_ref ref : fromClause.get_table_refs()) {
			
			table_ref_aux tableRefAux = ref.get_table_ref_aux();
			
			handlerTableRefAuxInFrom(tableRefAux, withclause);
			tableElementList.add(tableRefAux);
			
			List<join_clause> joinList = ref.get_join_clauses();
			for(join_clause join : joinList) {
				tableRefAux = join.get_table_ref_aux();
				handlerTableRefAuxInFrom(tableRefAux, withclause);
			}
		}
		
		//start clause
		start_part startClause = connectionClause.get_start_part();
		
		/* make the query in with clause start */
		factoring_element factoringElement = new factoring_element();
		//query name in with clause
		String strQueryName = "cte_" + (withclause.get_factoring_elements().size());
		query_name queryName = new query_name();
		id queryNameId = new id();
		queryNameId.set_value(strQueryName);
		queryName.set_id(queryNameId);
		factoringElement.set_query_name(queryName);
		
		subquery subquery = new subquery();
		query_block queryBlock = new query_block();
		//from clause
		queryBlock.set_from_clause(fromClause);
		//select list -> *
		projection_asterisk asterisk = new projection_asterisk();
		queryBlock.set_query_block_projection(asterisk);
		if(startClause != null) {
			where_clause whereClause = new where_clause();
			whereClause.set_expression(startClause.get_expression());
			queryBlock.set_where_clause(whereClause);
		}
		subquery.set_subquery_basic_elements(queryBlock);
		//union in query
		subquery_operation_union union = new subquery_operation_union();
		
		//union all
		CommonToken t = new CommonToken(PLSQLPrinter.SQL92_RESERVED_ALL);
		t.setText("ALL");
		CommonTree commonTree = new CommonTree(t);
		union.set_SQL92_RESERVED_ALL(commonTree);
		
		query_block unionBlock = new query_block();
		
		//make from clause in union query 
		from_clause unionFrom = new from_clause();
		//TODO
		table_ref tableRef = fromClause.get_table_refs().get(0);
		table_ref_aux tableRefAux = tableRef.get_table_ref_aux();
		dml_table_expression_clause tableexpressionclause = tableRefAux.get_dml_table_expression_clause();
		alias kalias = tableRefAux.get_alias();
		if(kalias == null) {
			// If alias is not exist, 'k' is setted 
			kalias = new table_alias();
			id id = new id();
			id.set_value("k");
			((table_alias)kalias).set_id(id);
		}
		tableRef = new table_ref();
		tableRefAux = new table_ref_aux();
		tableRefAux.set_dml_table_expression_clause(tableexpressionclause);
		tableRefAux.set_alias(kalias);
		tableRef.set_table_ref_aux(tableRefAux);
		unionFrom.add_table_refs(tableRef);
		//table cte
		tableRef = new table_ref();
		tableRefAux = new table_ref_aux();
		table_alias cteAlias = new table_alias();
		id cid = new id();
		cid.set_value("c");
		cteAlias.set_id(cid);
		tableRefAux.set_alias(cteAlias);
		table_expression tableExp = new table_expression();
		general_element generalElement = new general_element();
		general_element_id generalElementId = new general_element_id();
		generalElementId.set_id(queryNameId);
		generalElement.add_general_element_items(generalElementId);
		tableExp.set_table_expression_element(generalElement);
		tableRefAux.set_dml_table_expression_clause(tableExp);
		tableRef.set_table_ref_aux(tableRefAux);
		unionFrom.add_table_refs(tableRef);
		unionBlock.set_from_clause(unionFrom);
		//make where in union clause
		where_clause unionWhere = new where_clause();
		expression_element_eq expEq = new expression_element_eq();
		generalElement = new general_element();
		generalElementId = new general_element_id();
		generalElementId.set_id(cid);
		generalElement.add_general_element_items(generalElementId);
		generalElement.add_general_element_items(priorExp);
		expEq.set_lhs(generalElement);
		generalElement = new general_element();
		generalElementId = new general_element_id();
		generalElementId.set_id(((table_alias)kalias).get_id());
		generalElement.add_general_element_items(generalElementId);
		generalElement.add_general_element_items(notPriorExp);
		expEq.set_rhs(generalElement);
		logic_expression logicExp = new logic_expression();
		logicExp.set_expression_element(expEq);
		unionWhere.set_expression(logicExp);
		unionBlock.set_where_clause(unionWhere);
		
		//select list
		projection_list unionSelect = new projection_list();
		selected_element selectElement = new selected_element();
		expression_element_dot_asterisk dotasterisk = new expression_element_dot_asterisk();
		tableview_name tableName = new tableview_name();
		tableName.add_ids(((table_alias)kalias).get_id());
		dotasterisk.set_tableview_name(tableName);
		general_expression generalExp = new general_expression();
		generalExp.set_expression_element(dotasterisk);
		selectElement.set_expression(generalExp);
		unionSelect.add_selected_elements(selectElement);
		unionBlock.set_query_block_projection(unionSelect);

		union.set_subquery_basic_elements(unionBlock);
		subquery.add_subquery_operation_parts(union);
		
		factoringElement.set_subquery(subquery);
		
		withclause.add_factoring_elements(factoringElement);
		/* make the query in with clause end */

		return true;
	}
    
}
