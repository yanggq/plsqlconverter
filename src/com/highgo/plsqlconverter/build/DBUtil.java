package com.highgo.plsqlconverter.build;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DBUtil {
	private static String DRIVER_CLASS;
	
	private static String JDBC_URL;
	
	private static String DB_USER;
	
	private static String DB_PASSWORD;
	
	static {
		Properties prop = new Properties();
		InputStream is = DBUtil.class.getClassLoader().getResourceAsStream("dbsetting.properties");
		
		try {
			prop.load(is);
			DRIVER_CLASS = prop.getProperty("db.driverClass");
			JDBC_URL = prop.getProperty("db.jdbcUrl");
			DB_USER = prop.getProperty("db.user");
			DB_PASSWORD = prop.getProperty("db.password");
		} catch (IOException e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static Connection getConnection() throws SQLException, ClassNotFoundException {
		Connection conn = null;
		Class.forName(DRIVER_CLASS);
		conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASSWORD);
		return conn;
	}
	
	public static void rollBack(Connection conn) {
		try {
			if(conn != null) {
				conn.rollback();
			}
		} catch (SQLException e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static void close(Connection conn) {
		try {
			if(conn != null) {
				conn.close();
				conn = null;
			}
		} catch (SQLException e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static void close(Statement stmt) {
		try {
			if(stmt != null) {
				stmt.close();
				stmt = null;
			}
		} catch (SQLException e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static void close(ResultSet rs ) {
		try {
			if(rs != null) {
				rs.close();
				rs = null;
			}
		} catch (SQLException e) {
			e.printStackTrace(System.out);
		}
	}
}
