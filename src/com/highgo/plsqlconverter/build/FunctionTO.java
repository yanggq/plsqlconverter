package com.highgo.plsqlconverter.build;

import java.util.List;

public class FunctionTO {
	
	private String schemaName;
	
	private String functionName;
	
	private String functionBody;
	
	/**
	 * Function type contains function converted from package,function converted from procedure 
	 * and function converted from function.
	 */
	private String functionType ;
	
	private List<String> errorMsgList;
	
	

	public FunctionTO() {
		super();
	}

	public FunctionTO(String schemaName, String functionName, String functionBody,String functionType, List<String> errorMsgList) {
		super();
		this.schemaName = schemaName;
		this.functionName = functionName;
		this.functionBody = functionBody;
		this.functionType = functionType;
		this.errorMsgList = errorMsgList;
	}

	public String getSchemaName() {
		return schemaName;
	}

	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getFunctionBody() {
		return functionBody;
	}

	public void setFunctionBody(String functionBody) {
		this.functionBody = functionBody;
	}
	
	public String getFunctionType() {
		return functionType;
	}

	public void setFunctionType(String functionType) {
		this.functionType = functionType;
	}

	public List<String> getErrorMsgList() {
		return errorMsgList;
	}

	public void setErrorMsgList(List<String> errorMsgList) {
		this.errorMsgList = errorMsgList;
	}
	
	public static final String PACKAGE_FUNTION = "package-function";
	
	public static final String PROCEDURE_FUNCTION = "procedure-function";
	
	public static final String FUNCTION_FUNCTION = "function-function";
	
	
}
