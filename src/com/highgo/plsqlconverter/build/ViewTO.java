package com.highgo.plsqlconverter.build;

public class ViewTO {
	private String viewName;
	
	private String viewBody;
	
	private String errorMsg;

	public ViewTO(String viewName, String viewBody, String errorMsg) {
		super();
		this.viewName = viewName;
		this.viewBody = viewBody;
		this.errorMsg = errorMsg;
	}

	public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	public String getViewBody() {
		return viewBody;
	}

	public void setViewBody(String viewBody) {
		this.viewBody = viewBody;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
	
}
