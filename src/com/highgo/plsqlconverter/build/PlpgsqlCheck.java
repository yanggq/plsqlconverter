package com.highgo.plsqlconverter.build;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.highgo.plsqlconverter.model.ProcedureInfoTO;
import com.highgo.plsqlconverter.view.Plsqlconverter;


public class PlpgsqlCheck {
	private static Logger logger = Logger.getLogger(PlpgsqlCheck.class);

	public static void main(String[] args) {
//		executeCommand("/home/zhangyc/postgresql10/bin/pg_ctl stop -D /home/zhangyc/postgresql10/data");
//		logger.info("test");
		//checkFunction();

	}
	
	
	
	/**
	 * Create all scheme by the package name in plsql.
	 * @param conn 
	 * @param packages
	 * @return Comma separated package name
	 * @throws Exception 
	 */
	public static String createSchemByPackageName(Connection conn, List<ProcedureInfoTO> packages) throws Exception {
		logger.info("Enter create schema by package name.");
		Statement stmt = null;
		StringBuffer stringBuffer = new StringBuffer();
		try {
			//conn.setAutoCommit(false);
			if(packages != null && packages.size() > 0) {
				
				for(ProcedureInfoTO to : packages) {
					stringBuffer.append(to.getObjectName()).append(",");
					//dropSchema(to.getObjectName(), conn);
					stmt = conn.createStatement();
					stmt.execute("create schema " + to.getObjectName());
					
				}
				
				//delete last comma
				stringBuffer.deleteCharAt(stringBuffer.lastIndexOf(","));
			} else {
				logger.info("scheme name list is null or empty.");
				return stringBuffer.toString();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
			
		} finally {
			DBUtil.close(stmt);
		}
		logger.info("finish create schema by package name.");
		return stringBuffer.toString();
	}
	
	/**
	 * Create table structure into pg database from script file provided by data migration tool.
	 * Doing this is read for creating function successfully
	 * Failed execute one sql ,then rollback.
	 * @param conn 
	 * @param scriptFilePath
	 * @return
	 */
	public static boolean createTableByScript(Connection conn, String scriptFilePath) {
		logger.info("Enter create table structure from script file.");
		//Connection conn = null;
		Statement stmt = null;
		FileReader fileReader = null;
		BufferedReader br = null;
		try {
			//conn = DBUtil.getConnection();
//			if(conn == null) {
//				logger.info("failed get connection for pg database");
//				return false;
//			}
			
			File scriptFile = new File(scriptFilePath);
			fileReader = new FileReader(scriptFile);
			br = new BufferedReader(fileReader);
			String sqlLine = null;
			while((sqlLine = br.readLine()) != null) {
				sqlLine = sqlLine.trim();
				if (sqlLine.isEmpty()) {
					continue;
				}
				if(sqlLine.endsWith(";")) {
					sqlLine = sqlLine.substring(0, sqlLine.length() - 1);
				}
				try {
					stmt = conn.createStatement();
					stmt.execute(sqlLine);
				} catch (Exception e) {
					logger.error("Create table fail :" + e.getMessage() + ":" + sqlLine);
					continue;
				}
				logger.info("sucessfully execute:" + sqlLine);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
			return false;
		} finally {
			DBUtil.close(stmt);
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace(System.out);
				}
			}
			if(fileReader != null) {
				try {
					fileReader.close();
				} catch (IOException e) {
					e.printStackTrace(System.out);
				}
			}
		}
		logger.info("finish create table from script file.");
		return true;
		
	}
	
	/**
	 * Create view into pg database order by plsql covert tool.
	 * @param conn 
	 * @param viewList
	 * @return
	 */
	public static List<ViewTO> craeteView(Connection conn, List<ProcedureInfoTO> viewList) {
		logger.info("enter handle view");
		Statement stmt = null;
		List<ViewTO> errorViewList = new ArrayList<>();
		try {
			stmt = conn.createStatement();
		
			if(viewList != null && viewList.size() > 0) {
				logger.info("view list size is:" + viewList.size());
				for(ProcedureInfoTO view : viewList) {
					
					if(view.isBuildError() || !view.isConverterFlg()) {
						continue;
					}
					
					String viewName = view.getObjectName();
					String viewBody = view.getConverterText();
					try {
						stmt.execute(viewBody);
					} catch (SQLException e) {
						String errorMsg = e.getMessage();
						logger.info(errorMsg);
						ViewTO errorView = new ViewTO(viewName, viewBody, errorMsg);
						errorViewList.add(errorView);
					}
				}
			}
			logger.info("finish create view ,error view size is:" + errorViewList.size());
			return errorViewList;
		} catch (SQLException e) {
			e.printStackTrace(System.out);
			return null;
		} finally {
			DBUtil.close(stmt);
		}
	}
	
	/**
	 * Create plpgsql(package,procedure,function) into pg database order by plsql convert tool.
	 * @param conn 
	 * @param pakcageList
	 * @param procedureList
	 * @param functionList
	 * @param errorFuncList out
	 * @return count of function
	 */
	public static int createFcuntion(Connection conn, List<ProcedureInfoTO> pakcageList,List<ProcedureInfoTO> procedureList,List<ProcedureInfoTO> functionList, List<FunctionTO> errorFuncList) {
		logger.info("enter create function from plsql convert tool.");
		Statement stmt = null;
		//List<FunctionTO> errorFuncList = new ArrayList<>();
		int count = 0;
		try {
			stmt = conn.createStatement();
			//handle package list
			if(pakcageList != null && pakcageList.size() > 0) {
				count = count + compileFunctionList(pakcageList, errorFuncList, stmt);
			} else {
				logger.info("procedure package list is null or empty.");
			}
			//handle procedure list
			if(procedureList != null && procedureList.size() > 0) {
				count = count + compileFunctionList(procedureList, errorFuncList, stmt);
			} else {
				logger.info("procedure procedure list is null or empty.");
			}
			//handle function list
			if(functionList != null && functionList.size() > 0) {
				count = count + compileFunctionList(functionList, errorFuncList, stmt);
			} else {
				logger.info("procedure function list is null or empty.");
			}
			
		} catch (SQLException e) {
			e.printStackTrace(System.out);
			return 0;
		} finally {
			DBUtil.close(stmt);
		}
		logger.info("finish create function,error function size is :" + errorFuncList.size());
		return count;
	}
	
	/**
	 * Compile function converted from procedure(package,function,procedur) into pg database by executing it.
	 * Return the failed function list.
	 * @param pakcageList
	 * @param failedFunctionList
	 * @param stmt
	 * @return count of function
	 */
	private static int compileFunctionList(List<ProcedureInfoTO> pakcageList,List<FunctionTO> failedFunctionList,Statement stmt) {
		int count = 0;
		for(ProcedureInfoTO procedure : pakcageList) {
			if(!procedure.isConverterFlg() || procedure.isBuildError()) {
				continue;
			}
			List<FunctionTO> functionList = convertProcedureToFunctionList(procedure);
			count = count + functionList.size();
			if(functionList.size() > 0) {
				for(FunctionTO function : functionList) {
					try {
						stmt.execute(function.getFunctionBody());
					} catch (SQLException e) {
						logger.info(e.getMessage());
						List<String> msgList = new ArrayList<>();
						msgList.add(e.getMessage());
						function.setErrorMsgList(msgList);
						failedFunctionList.add(function);
					}
				}
			} else {
				logger.info("cant not get function from source:" + procedure.getObjectName());
			}
		}
		
		return count;
	}
	
	/**
	 * Convert package to function list.
	 * @param procedure
	 * @return
	 */
	private static List<FunctionTO> convertProcedureToFunctionList(ProcedureInfoTO procedure) {
		logger.info("enter convert package to function list");
		String objectType = procedure.getObjectType();
		List<FunctionTO> functionList = new ArrayList<>();
		if(Plsqlconverter.OBJECT_TYPE_PACKAGE_BODY.equals(objectType)) {
			String packageName  = procedure.getObjectName();
			Pattern patternFunction = Pattern.compile("create\\s+or\\s+replace\\s+function\\s+" + packageName + "\\.(\\w+)\\s", 
					Pattern.CASE_INSENSITIVE);
			String procedureBodyText = procedure.getConverterText();
			Matcher matcherFunction = patternFunction.matcher(procedureBodyText);
	        int functionCount = 0;
	        int functionStartIndex = 0;
	        String functionName = "";
	        
	        while(matcherFunction.find()) {
	            functionCount++;
	            if(functionCount == 1) {
	            	functionName = matcherFunction.group(1);
	                continue;
	            }
	            
	            String functionBody = procedureBodyText.substring(functionStartIndex, matcherFunction.start());

				FunctionTO funtcionTO = new FunctionTO(packageName, functionName, functionBody,FunctionTO.PACKAGE_FUNTION, null);
				functionList.add(funtcionTO);
				
				functionStartIndex = matcherFunction.start();
				functionName = matcherFunction.group(1);
	        }
	        
	        String functionBody = procedureBodyText.substring(functionStartIndex);

			FunctionTO funtcionTO = new FunctionTO(packageName, functionName, functionBody,FunctionTO.PACKAGE_FUNTION, null);
			functionList.add(funtcionTO);
		} else if(Plsqlconverter.OBJECT_TYPE_PROCEDURE.equals(objectType)) {
			FunctionTO function = new FunctionTO(procedure.getObjectOwner(), procedure.getObjectName(), procedure.getConverterText(), FunctionTO.PROCEDURE_FUNCTION, null);
			functionList.add(function);
		} else if(Plsqlconverter.OBJECT_TYPE_FUNCTION.equals(objectType)) {
			FunctionTO function = new FunctionTO(procedure.getObjectOwner(), procedure.getObjectName(), procedure.getConverterText(), FunctionTO.FUNCTION_FUNCTION, null);
			functionList.add(function);
		}
		
        logger.info("function list size is " + functionList.size());
		return functionList;
	}
	
	/**
	 * get pg status
	 * @param pgpath
	 * @return int 0: running 3:not running
	 * */
	public static int getPgStatus(String pgPath) {
		String statusCommand = pgPath + "/bin/pg_ctl status -D " + pgPath + "/data";
		int status = executeCommand(statusCommand);
		
		return status;
	}
	
	/**
	 * Start inner pg database.If it not running,then start it; if it was running,then restart it.
	 * Finally ensure it's status is running.
	 * @param pgPath
	 * @return
	 * @throws Exception 
	 */
	public static boolean startPG(String pgPath) throws Exception {
		boolean res = false;
		String message = null;
		//String statusCommand = pgPath + "/bin/pg_ctl status -D " + pgPath + "/data";
		String startCommand = pgPath + "/bin/pg_ctl start -D " + pgPath + "/data";
		String restartCommand = pgPath + "/bin/pg_ctl restart -D " + pgPath + "/data -m immediate";
		int status = getPgStatus(pgPath);
		if(status == 3) {//status is 3 ,so the pg database is not running ,start it.
			if(executeCommand(startCommand) == 0) {
				res = true;
				message = "successfully start pg database.";
			} else {
				message = "failed start pg database ,please check it manual.";
			}
		} else if(status == 0) {//status is 0 ,so the pg database is running,restart it.
			if(executeCommand(restartCommand) == 0) {
				res = true;
				message = "successfully restart pg database.";
			} else {
				message = "failed restart pg database ,please check it manual.";
			}
		} else {
			message = "something wrong when checking pg database status,please check it manual.";
		}
		
		int index = 0;
		while(getPgStatus(pgPath) != 0) {
			index ++;
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			if(index > 300) { 
				throw new Exception("faild start pg database, unkown reason,please check it manual.");
			}
		}
		
		//check database status finally.
//		if(getPgStatus(pgPath) != 0) {
//			res = false;
//			message = "faild start pg database, unkown reason,please check it manual.";
//		}
		logger.info(message);//need show this message by dialog
		return res;
	}
	
	/**
	 * Shutdown inner pg database.If it not running ,return;if it was running,stop it.
	 * Finally ensure it's status is shutting down.
	 * @param pgPath
	 * @return
	 */
	public static boolean shutdownPG(String pgPath) {
		boolean res = false;
		String message = null;
		//String statusCommand = pgPath + "/bin/pg_ctl status -D " + pgPath + "/data";
		String stopCommand = pgPath + "/bin/pg_ctl stop -D " + pgPath + "/data" + " -m fast";
		int status = getPgStatus(pgPath);
		if(status == 0) {
			if(executeCommand(stopCommand) == 0) {
				res = true;
				message = "successfully stop pg database";
			} else {
				message = "failed stop pg database,please stop it manual";
			}
		} else if(status == 3) {
			res = true;
			message = "pg database had been stop.";
		} else {
			message = "something wrong when checking pg database status,please check it manual.";
		}
		if(getPgStatus(pgPath) == 0) {
			res = false;
			message = "faild stop pg database, unkown reason,please check it manual.";
		}
		logger.info(message);//need show this message by dialog
		return res;
	}
	
	
	/**
	 * Check functions created in the pg database,return the functions contain error.
	 * @return List<FunctionTO>
	 */
	public static List<FunctionTO> checkFunction(boolean fatal_errors, boolean others_warnings, boolean performance_warnings, boolean extra_warnings) {
		logger.info("enter check function");
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		List<FunctionTO> errorFunctionList = new ArrayList<>();
		String checkSQL = "SELECT " + 
				"    (pcf).functionid::regprocedure, (pcf).lineno, (pcf).statement," + 
				"    (pcf).sqlstate, (pcf).message, (pcf).detail, (pcf).hint, (pcf).level," + 
				"    (pcf).\"position\", (pcf).query, (pcf).context,proname::varchar,prooid::varchar" + 
				" FROM " + 
				"(" + 
				"    SELECT " + 
				"        plpgsql_check_function_tb(pg_proc.oid, COALESCE(pg_trigger.tgrelid, 0)";
				if(fatal_errors) {
					checkSQL = checkSQL + ", fatal_errors := false";
				}
				if (!others_warnings) {
					checkSQL = checkSQL + ", others_warnings := false";
				}
				if (performance_warnings) {
					checkSQL = checkSQL + ", performance_warnings := true";
				}
				if (!extra_warnings) {
					checkSQL = checkSQL + ", extra_warnings := false";
				}
				checkSQL = checkSQL + ") AS pcf,pg_proc.proname as proname,pg_proc.oid as prooid" + 
				"    FROM pg_proc" + 
				"    LEFT JOIN pg_trigger" + 
				"        ON (pg_trigger.tgfoid = pg_proc.oid)" + 
				"    WHERE" + 
				"        prolang = (SELECT lang.oid FROM pg_language lang WHERE lang.lanname = 'plpgsql') AND" + 
				"        pronamespace <> (SELECT nsp.oid FROM pg_namespace nsp WHERE nsp.nspname = 'pg_catalog') AND" + 
				"        (pg_proc.prorettype <> (SELECT typ.oid FROM pg_type typ WHERE typ.typname = 'trigger') OR" + 
				"         pg_trigger.tgfoid IS NOT NULL)" + 
				"    OFFSET 0" + 
				") ss " + 
				"ORDER BY (pcf).functionid::regprocedure::text, (pcf).lineno";
		
		String getFunctionSQL = "select pg_get_functiondef(?) as funcdef,pg_namespace.nspname from pg_proc,"
				+ "pg_namespace where pg_proc.pronamespace = pg_namespace.oid and pg_proc.oid = ?";
		try {
			conn = DBUtil.getConnection();
			if(conn == null) {
				logger.info("failed get connection for pg database");
				return null;
			}
			//check the functions ,generate the functionTO map,
			//function oid as map key,functionTO as map value
			stmt = conn.createStatement();
			rs = stmt.executeQuery(checkSQL);
			Map<String,FunctionTO> functionMap = new HashMap<>();
			FunctionTO function = null;
			String tmpOid = null;
			String oidKey = null;
			while(rs.next()) {
				oidKey = rs.getString("prooid");
				String proname = rs.getString("proname");
				String statementType = rs.getString("statement");
				int lineNo = rs.getInt("lineno");
				String mesage = rs.getString("message");
				String hint = rs.getString("hint");
				String level = rs.getString("level");
				int position = rs.getInt("position");
				String query = rs.getString("query");
				String context = rs.getString("context");
				StringBuilder sb = new StringBuilder();
				//make message content
				sb.append("<b>");
				sb.append(level).append(":");
				sb.append("</b>");
				sb.append(mesage);
				if(statementType != null && !statementType.isEmpty()) {
					sb.append(";").append("statement type:").append(statementType);
				}
				if(hint != null && !hint.isEmpty()) {
					sb.append(";").append("hint:").append(hint);
				}
				if(query != null && !query.isEmpty()) {
					sb.append(";").append("query:").append(query);
				}
				if(context != null && !context.isEmpty()) {
					sb.append(";").append("context:").append(context);
				}
				if(lineNo != 0) {
					sb.append(";").append("lineNo:").append(lineNo);
				}
				if(position != 0) {
					sb.append(";").append("position:").append(position);
				}
				logger.debug(sb.toString());
				
				if(function == null) {
					function = new FunctionTO();
					function.setFunctionName(proname);
					List<String> errorMsgList = new ArrayList<>();
					
					errorMsgList.add(sb.toString());
					function.setErrorMsgList(errorMsgList);
				} else {
					if(!tmpOid.equals(oidKey)) {
						functionMap.put(tmpOid, function);
						function = new FunctionTO();
						function.setFunctionName(proname);
						List<String> errorMsgList = new ArrayList<>();
						errorMsgList.add(sb.toString());
						function.setErrorMsgList(errorMsgList);
					} else {
						function.getErrorMsgList().add(sb.toString());
					}
				}
				tmpOid = oidKey;
			}
			if(oidKey != null && function != null) {
				functionMap.put(tmpOid, function);
			}
			//get function body and schema name by function oid from function map.
			Set<String> keySet = functionMap.keySet();
			if(keySet.size() > 0) {
				Iterator<String> it = keySet.iterator();
				while(it.hasNext()) {
					String key = it.next();
					int keyInt = Integer.parseInt(key);
					FunctionTO functionTO = functionMap.get(key);
					if(functionTO != null) {
						pStmt = conn.prepareStatement(getFunctionSQL);
						pStmt.setInt(1, keyInt);
						pStmt.setInt(2, keyInt);
						rs = pStmt.executeQuery();
						if(rs.next()) {
							String funcionBody = rs.getString("funcdef");
							String functionSchema = rs.getString("nspname");
							functionTO.setFunctionBody(funcionBody);
							functionTO.setSchemaName(functionSchema);
						}
						
						errorFunctionList.add(functionTO);
					}
				}
				
			} else {
				logger.info("can not find error in the function when checking it.");
				return null;
			}
			
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace(System.out);
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(stmt);
			DBUtil.close(pStmt);
			DBUtil.close(conn);
		}
//		for(FunctionTO f: errorFunctionList) {
//			System.out.println(f.getFunctionName());
//		}
		logger.info("finish check function in pg database");
		return errorFunctionList;
	}
	
	/**
	 * Execute system command by runtime.
	 * @param command
	 * @return
	 */
	public static int executeCommand(String command) {
		int exitValue = -1;
		Runtime runtime = Runtime.getRuntime();
		try {
			logger.info(command);
			Process p = runtime.exec(command);
			p.waitFor();
			exitValue = p.exitValue();
			logger.info("exit value is " + exitValue);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return exitValue;
	}


	/**
	 * Create scheme by name in plsql.
	 * @param conn 
	 * @param schema name
	 * @return
	 */
	public static boolean createSchema(Connection conn, String schemaName) {
		
		logger.info("Enter create scheme by name.");
		Statement stmt = null;
		try {
			//conn.setAutoCommit(false);
			//dropSchema(schemaName, conn);
			
			stmt = conn.createStatement();
			stmt.execute("create schema " + schemaName);

		} catch (SQLException e) {
			//e.printStackTrace(System.out);
			//DBUtil.rollBack(conn);
			logger.error(e.getMessage());
			return false;
			
		} finally {
			DBUtil.close(stmt);
		}
		logger.info("finish create schema.");
		return true;
	}
	
	/**
	 * drop schema if exist
	 * @param schema name
	 * @param connection
	 * */
//	private  static void dropSchema(String schemaName, Connection conn) {
//		String sql = "DROP SCHEMA IF EXISTS " + schemaName + " CASCADE";
//		
//		Statement stmt = null;
//		try {
//			stmt = conn.createStatement();
//			stmt.execute(sql);
//		} catch (SQLException e) {
//			logger.error(e.getMessage());
//			
//		} finally {
//			DBUtil.close(stmt);
//		}
//		
//	}
	
	

}
