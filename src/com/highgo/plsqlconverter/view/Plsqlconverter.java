/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.highgo.plsqlconverter.view;

import com.highgo.plsqlconverter.build.DBUtil;
import com.highgo.plsqlconverter.build.FunctionTO;
import com.highgo.plsqlconverter.build.PlpgsqlCheck;
import com.highgo.plsqlconverter.model.ConnInfoTO;
import com.highgo.plsqlconverter.model.ProcedureHtmlTO;
import com.highgo.plsqlconverter.model.ProcedureInfoTO;
import com.highgo.plsqlconverter.util.CommonUtil;
import com.highgo.plsqlconverter.util.FileUtil;
import com.highgo.plsqlconverter.util.HtmlUtil;
import com.highgo.plsqlconverter.util.JdbcHelper;
import com.highgo.plsqlconverter.util.JdbcHelper.DBType;
import java.awt.CardLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import ru.barsopen.plsqlconverter.Main;

/**
 *
 * @author yanggq
 */
public class Plsqlconverter extends javax.swing.JFrame {
    
	private static final long serialVersionUID = 1L;

	private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    private ResourceBundle messageBundle = ResourceBundle.getBundle("message");
    
    private CardLayout card;
    
    private int index;
    
    public final static String OBJECT_TYPE_PROCEDURE = "PROCEDURE";
    public final static String OBJECT_TYPE_FUNCTION = "FUNCTION";
    public final static String OBJECT_TYPE_PACKAGE_BODY = "PACKAGE BODY";
    public final static String OBJECT_TYPE_PACKAGE = "PACKAGE";
    public static final String OBJECT_TYPE_VIEW = "VIEW";
    
    private List<ProcedureInfoTO> packageDelare = new ArrayList<>();
    private List<ProcedureInfoTO> procedures = new ArrayList<>();
    private List<ProcedureInfoTO> functions = new ArrayList<>();
    private List<ProcedureInfoTO> packages = new ArrayList<>();
    private List<ProcedureInfoTO> views = new ArrayList<>();
    
    private int procedureSuccessCount = 0;
    private int functionSuccessCount = 0;
    private int packageSuccessCount = 0;
    private int viewSuccessCount = 0;
    private int procedureBuildErrorCount = 0;
    private int functionBuildErrorCount = 0;
    private int packageBuildErrorCount = 0;
    private int viewBuildErrorCount = 0;
    
    private String schemaName;
    
    private String pgpath = "pg10";
    
    private boolean buildNextFlg = false;

    /**
     * Creates new form Plsqlconverter
     * @throws Exception 
     */
    public Plsqlconverter() {
        initComponents();
        this.customView();
        this.addListener();
        this.initComponent();
        
        //The frame is placed in the middle
        this.setLocationRelativeTo(null);
        this.setTitle(constBundle.getString("title"));
        Toolkit tool = this.getToolkit();
        this.setIconImage(tool.getImage(Plsqlconverter.class.getClassLoader().getResource("hgdb32.png")));
        dagCheck.setIconImage(tool.getImage(Plsqlconverter.class.getClassLoader().getResource("hgdb32.png")));

//        txtSourceIp.setText("192.168.100.208");
//        txtSourceServerName.setText("icbc");
//        txtSourceUserName.setText("lcicbc9999");
//        txtSourcePassword.setText("aaaaaa");
        
        //start temp pg
        try {
			PlpgsqlCheck.startPG(pgpath);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

        //add listener of window close 
        addWindowListener(new WindowAdapter() {
        	public void windowClosing(WindowEvent e) {
        		PlpgsqlCheck.shutdownPG(pgpath);
        	}
		});
    }
    
    private void initComponent(){
        index = 0;
        btnNext.setEnabled(true);
        btnBackup.setEnabled(false);
        brnFininsh.setEnabled(false);
    }
    
    /**
    * add listener 
     */
    private void addListener(){
        btnNext.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnNextAction(e);
            }
        });
        
        brnFininsh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnNextAction(e);
            }
        });
        
        btnBackup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnBackUpAction(e);
            }
        });
        
        btnSourceTestConnection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                testConnection(DBType.ORACLE);
            }
        });
        
		dagCheck.setLocationRelativeTo(this);
        btnPgCheck.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//chxFatalErrors.setSelected(true);
				chxOtherWarnings.setSelected(false);
				chxExtraWarnings.setSelected(false);
				chxPerformanceWarnings.setSelected(false);
				dagCheck.setVisible(true);
				//pgcheck();
			}
		});
        
        btnCheckExecute.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				pgcheck();
				dagCheck.setVisible(false);
			}
		});
        
        btnCheckCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dagCheck.setVisible(false);
			}
		});
        
    }
    
    /**
     * check
     * */
    private void pgcheck() {
    	List<FunctionTO> errorList = PlpgsqlCheck.checkFunction(true, chxOtherWarnings.isSelected(), chxPerformanceWarnings.isSelected(), chxExtraWarnings.isSelected());
    	
    	 //make html
        for(FunctionTO function : errorList) {
        	HtmlUtil.createSub(function.getFunctionBody(), function.getFunctionName());
        }
        
        HtmlUtil.createHtml(errorList,"checkFunction",  errorList.size(), false);
    }
    
    /*
    * test connection
    */
    private void testConnection(DBType type){
        ConnInfoTO conninfo = null;
        switch(type) {
            case ORACLE:
                conninfo = new ConnInfoTO(txtSourceIp.getText().trim(),
                                          Integer.parseInt(txtSourcePort.getValue().toString()),
                                          txtSourceUserName.getText().trim(),
                                          String.valueOf(txtSourcePassword.getPassword()),
                                          txtSourceServerName.getText().trim(), null);
                break;
            case POSTGRESQL:
                break;
        }
        Connection connection = null;
        try {
            connection = JdbcHelper.getConnection(type, conninfo);
        } catch (Exception ex) {
           JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorTitle"), JOptionPane.ERROR_MESSAGE);
           return;
        }
        if(connection == null) {
            JOptionPane.showMessageDialog(this, constBundle.getString("errorMessage"), constBundle.getString("errorTitle"), JOptionPane.ERROR_MESSAGE);
            return;
        } else {
            JOptionPane.showMessageDialog(this, constBundle.getString("connectMessage"), constBundle.getString("connectMessage"), JOptionPane.INFORMATION_MESSAGE);
            JdbcHelper.close(connection);
        }
    }
    
    private void btnBackUpAction(ActionEvent e){
        index--;
        brnFininsh.setEnabled(false);
        btnNext.setEnabled(false);
        btnBackup.setEnabled(false);
        

        if(buildNextFlg) {
        	buildNextFlg = false;
        	brnFininsh.setEnabled(true);
            btnNext.setEnabled(true);
            return;
        }

        
        card.previous(pnlMain);
        treeNavigation.setSelectionRow(index);
        if(index == 0) {
            btnNext.setEnabled(true);
        }

        if(index == 1) {
            brnFininsh.setEnabled(true);
            btnBackup.setEnabled(true);
            brnFininsh.setText(constBundle.getString("finish"));
        }
        
        if(index == 2) {
        	brnFininsh.setEnabled(true);
        	btnBackup.setEnabled(true);
        }
    }
    
    /**
     * next action
     * */
    private void btnNextAction(ActionEvent e){
    	
    	if(!buildNextFlg) {
    		index++;
    	}
        brnFininsh.setEnabled(false);
        
        if(index == 1) {
            if(!getProcedure()){
                index = 0;
                return;
            }
            
            btnNext.setEnabled(false);
            brnFininsh.setEnabled(true);
        }
        if(index == 2) {
            executeConverter();

            btnNext.setEnabled(false);
            brnFininsh.setEnabled(true);
        }
        
        if(index == 3) {
        	btnNext.setEnabled(false);
        	brnFininsh.setEnabled(false);
        	if(buildNextFlg) {
        		createFunctionInPGNext();
        		buildNextFlg = false;
        	} else {
        		if(createFunctionInPG()) {;
        			return;
        		}
        	}
        }
        
        //change Jpanel 
        card.next(pnlMain);
        //chagne navigation tree 
        treeNavigation.setSelectionRow(index);
        
        btnBackup.setEnabled(true);
        
        if(index == 2) {
            afterConverterExecute();
            brnFininsh.setText(constBundle.getString("createFunction"));
        }
    }
    
    /*
     * crate function in pg
     * 1.create table by sql file
     * 2.create view 
     * 3.create schema by package
     * */
    private boolean createFunctionInPG(){
    	Connection conn = null;
    	
    	//clean result
        DefaultTableModel model = (DefaultTableModel)tblCreateFunctionResult.getModel();
        model.setRowCount(0);
    	
    	try {
    		conn = DBUtil.getConnection();
    		
    		String packageNames = null;
    		try {
    			packageNames = PlpgsqlCheck.createSchemByPackageName(conn, packages);
    		} catch (Exception e) {
    			//error have when create schema 
				JOptionPane.showMessageDialog(this, e.getMessage(), constBundle.getString("errorTitle"), JOptionPane.ERROR_MESSAGE);
				return false;
			}
    		
    		//create view
    		PlpgsqlCheck.craeteView(conn, views);
    		
    		//set search path
    		String searchPath = schemaName + "," + packageNames;
    		FileUtil.makeFile("searchPath", "searchPath.txt", searchPath);
    		
    		//prompt to set up search path in pg
    		JOptionPane.showMessageDialog(this, messageBundle.getString("searchPathMessage"), constBundle.getString("infotitle"), JOptionPane.INFORMATION_MESSAGE);
    		
    		btnNext.setEnabled(true);
    		buildNextFlg = true;
    	} catch (Exception e) {
			logger.error(e.getMessage());
		}finally {
			if(conn != null) {
				DBUtil.close(conn);
			}
		}
    	
    	return true;
    }
    
    /**
     * create function in temp pg
     * */
    private void createFunctionInPGNext() {
    	Connection conn = null;
    	try {
    		//resatr pg
    		PlpgsqlCheck.startPG(pgpath);
    		
    		//after restart pg get connection
    		int loop = 0;
    		while(true) {
    			loop ++;
    			try {
    				conn = DBUtil.getConnection();
    				break;
				} catch (Exception e) {
					if(loop > 300) { 
	    				throw new Exception("get Connection fail,please check pg status.");
	    			}
					Thread.sleep(100);
				}
    		}

    		//create function
    		List<FunctionTO> errorFuncList = new ArrayList<>();
    		int functionCount = PlpgsqlCheck.createFcuntion(conn, 
    								packages, 
    								procedures, 
    								functions, 
    								errorFuncList);
    		
    		
    		//show result after create function
            DefaultTableModel model = (DefaultTableModel)tblCreateFunctionResult.getModel();
            model.setRowCount(0);
            
            Object[] rnt = new Object[4];
            rnt[0] = OBJECT_TYPE_FUNCTION;
            rnt[1] = functionCount - errorFuncList.size();
            rnt[2] = errorFuncList.size();
            rnt[3] = functionCount;  
            model.addRow(rnt);
            //make html
            for(FunctionTO function : errorFuncList) {
            	HtmlUtil.createSub(function.getFunctionBody(), function.getFunctionName());
            }
            
            HtmlUtil.createHtml(errorFuncList, "buildFunction", functionCount, true);
    		
    	} catch (Exception e) {
			logger.error(e.getMessage());;
		}finally {
			if(conn != null) {
				DBUtil.close(conn);
			}
		}
    	
    }
    
    /*
    * make file(sql, html) after converter
    */
    private void afterConverterExecute(){
        //make sql file
        FileUtil.makeFile(procedures);
        FileUtil.makeFile(functions);
        FileUtil.makeFile(packages);
        FileUtil.makeFile(views);
        
        FileUtil.makeFileAll(procedures, functions, packages, views);
        
        // make html file 
        List<ProcedureHtmlTO> list = new ArrayList<>();

        List<ProcedureInfoTO> successList = new ArrayList<>();
        List<ProcedureInfoTO> failList = new ArrayList<>();
        List<ProcedureInfoTO> buildErrorList = new ArrayList<>();
        for(ProcedureInfoTO info : procedures) {
            if(info.isConverterFlg()) {
                successList.add(info);
            } else {
                if(info.isBuildError()) {
                    buildErrorList.add(info);
                } else {
                    failList.add(info);
                    HtmlUtil.createSub(info.getObjectText(), info.getObjectName());
                }
            }
        }
        
        ProcedureHtmlTO to = new ProcedureHtmlTO();
        to.setTypeName(OBJECT_TYPE_PROCEDURE);
        to.setSuccessCount(procedureSuccessCount);
        to.setFailCount(failList.size());
        to.setFailList(failList);
        to.setBuildErrorCount(buildErrorList.size());
        to.setBuildErrorList(buildErrorList);
        to.setSuccessList(successList);
        list.add(to);
        
        successList = new ArrayList<>();
        failList = new ArrayList<>();
        buildErrorList = new ArrayList<>();
        for(ProcedureInfoTO info : functions) {
            if(info.isConverterFlg()) {
                successList.add(info);
            } else {
                if(info.isBuildError()) {
                    buildErrorList.add(info);
                } else {
                    failList.add(info);
                    HtmlUtil.createSub(info.getObjectText(), info.getObjectName());
                }
            }
        }
        
        to = new ProcedureHtmlTO();
        to.setTypeName(OBJECT_TYPE_FUNCTION);
        to.setSuccessCount(functionSuccessCount);
        to.setFailCount(failList.size());
        to.setFailList(failList);
        to.setSuccessList(successList);
        to.setBuildErrorCount(buildErrorList.size());
        to.setBuildErrorList(buildErrorList);
        list.add(to);
        
        successList = new ArrayList<>();
        failList = new ArrayList<>();
        buildErrorList = new ArrayList<>();
        for(ProcedureInfoTO info : packages) {
            if(info.isConverterFlg()) {
                successList.add(info);
            } else {
                if(info.isBuildError()) {
                    buildErrorList.add(info);
                } else {
                    failList.add(info);
                    HtmlUtil.createSub(info.getObjectText(), info.getObjectName());
                }
            }
        }
        
        to = new ProcedureHtmlTO();
        to.setTypeName(OBJECT_TYPE_PACKAGE_BODY);
        to.setSuccessCount(packageSuccessCount);
        to.setFailCount(failList.size());
        to.setFailList(failList);
        to.setSuccessList(successList);
        to.setBuildErrorCount(buildErrorList.size());
        to.setBuildErrorList(buildErrorList);
        list.add(to);
        
        successList = new ArrayList<>();
        failList = new ArrayList<>();
        buildErrorList = new ArrayList<>();
        for(ProcedureInfoTO info : views) {
            if(info.isConverterFlg()) {
                successList.add(info);
            } else {
                if(info.isBuildError()) {
                    buildErrorList.add(info);
                } else {
                    failList.add(info);
                    String body = "CREATE OR REPLACE VIEW " + info.getObjectOwner() + "." + info.getObjectName() + " AS \n" + info.getObjectText().trim() + ";";
                    HtmlUtil.createSub(body, info.getObjectName());
                }
            }
        }
        
        to = new ProcedureHtmlTO();
        to.setTypeName(OBJECT_TYPE_VIEW);
        to.setSuccessCount(viewSuccessCount);
        to.setFailCount(failList.size());
        to.setFailList(failList);
        to.setSuccessList(successList);
        to.setBuildErrorCount(buildErrorList.size());
        to.setBuildErrorList(buildErrorList);
        list.add(to);
        
        HtmlUtil.create(list, schemaName);
    }
    
    /*
    * converter plsql and show result
    */
    private void executeConverter(){
        plsqlconverter();
        
        DefaultTableModel model = (DefaultTableModel)tblResult.getModel();
        model.setRowCount(0);
        
        Object[] rnt = new Object[5];
        rnt[0] = OBJECT_TYPE_PROCEDURE;
        rnt[1] = procedureSuccessCount;
        rnt[2] = procedures.size() - procedureSuccessCount - procedureBuildErrorCount;
        rnt[3] = procedureBuildErrorCount;
        rnt[4] = procedures.size();   
        model.addRow(rnt);
        
        rnt = new Object[5];
        rnt[0] = OBJECT_TYPE_FUNCTION;
        rnt[1] = functionSuccessCount;
        rnt[2] = functions.size() - functionSuccessCount - functionBuildErrorCount;
        rnt[3] = functionBuildErrorCount;
        rnt[4] = functions.size();   
        model.addRow(rnt);
        
        rnt = new Object[5];
        rnt[0] = OBJECT_TYPE_PACKAGE_BODY;
        rnt[1] = packageSuccessCount;
        rnt[2] = packages.size() - packageSuccessCount - packageBuildErrorCount;
        rnt[3] = packageBuildErrorCount;
        rnt[4] = packages.size();
        model.addRow(rnt);
        
        rnt = new Object[5];
        rnt[0] = OBJECT_TYPE_VIEW;
        rnt[1] = viewSuccessCount;
        rnt[2] = views.size() -viewSuccessCount - viewBuildErrorCount;
        rnt[3] = viewBuildErrorCount;
        rnt[4] = views.size();   
        model.addRow(rnt);
    }
    
    
    private void getPackageType(String packageText, String packageName, Map<String, Map<String, String>> packageTypeMap){
        if(packageText == null || packageText.isEmpty()) {
            return;
        }
        
        Pattern pattern = Pattern.compile("\\s+type\\s+(\\w+)\\s+is\\s+([\\w\\s]+)([\\S\\s]*?);", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(packageText);
        
        String typeNameNew = "";
        String typeName = "";
        
        Map<String,String> typeMap = new HashMap<>();
        
        while(matcher.find()) {
            typeNameNew = matcher.group(1);
            typeName = matcher.group(2);
            
            typeMap.put(typeNameNew, CommonUtil.typeOracle2Pg(typeName.trim()));
        }
        
        if(typeMap.size() == 0) {
            return;
        }
        
        packageTypeMap.put(packageName, typeMap);
    }
    /**
     * converter plsql to plpgsql
     */
    private void plsqlconverter(){
        Main convertMain = new Main();
        procedureSuccessCount = 0;
        functionSuccessCount = 0;
        packageSuccessCount = 0;
        viewSuccessCount = 0;
        packageBuildErrorCount = 0;
        procedureBuildErrorCount = 0;
        functionBuildErrorCount = 0;
        viewBuildErrorCount = 0;
        
        Map<String, Map<String, String>> packageTypeMap = new HashMap<>();
        
        for(ProcedureInfoTO to: packageDelare) {
//            if(to.isBuildError()) {
//                continue;
//            }
            getPackageType(to.getObjectText(),to.getObjectName(), packageTypeMap);
        }
        
        for(ProcedureInfoTO to : packages) {
            logger.info("-------------packageName:" + to.getObjectName() + "-------------start");
            if(to.isBuildError()) {
                packageBuildErrorCount++;
                continue;
            }
            String conBeforeText = "";
            try {
                
                conBeforeText = CommonUtil.handBeforConverter(to.getObjectText());
                String outSql = convertMain.convert(conBeforeText);
                if(outSql != null) {
                    String text = managerConverterOut(outSql, to.getObjectName());
                    text = managerPackageSpecialText(text, to.getObjectName(), packageTypeMap);
                    text = handSpecialText(text);
                    
                    to.setConverterText(text);
                    to.setConverterFlg(true);
                    packageSuccessCount++;
  
                } else {
                    to.setConverterFlg(false);
                    FileUtil.makefile2(to.getObjectName(), conBeforeText);
                }
            } catch(Exception ex) {
                //Logger.getLogger(Plsqlconverter.class.getName()).log(Level.SEVERE, null, ex);
                logger.error(ex.getMessage());
                to.setConverterFlg(false);
                to.setMessage(ex.getMessage());
                FileUtil.makefile2(to.getObjectName(), conBeforeText);
            }
        }
        
        for(ProcedureInfoTO to : procedures) {
            logger.info("-------------procedure:" + to.getObjectName() + "------------- start");
            if(to.isBuildError()) {
                procedureBuildErrorCount++;
                continue;
            }
            String conBeforeText = "";
            try {
                conBeforeText = CommonUtil.handBeforConverter(to.getObjectText());
                String outSql = convertMain.convert(conBeforeText);
                if(outSql != null) {
                    String text = managerConverterOut(outSql, to.getObjectName());
                    
                    text = handPackageType(text, packageTypeMap);
                    
                    text = handSpecialText(text);
                    to.setConverterText(text);
                    to.setConverterFlg(true);
                    procedureSuccessCount++;
                } else {
                    to.setConverterFlg(false);
                    FileUtil.makefile2(to.getObjectName(), conBeforeText);
                }
            } catch(Exception ex) {
                //Logger.getLogger(Plsqlconverter.class.getName()).log(Level.SEVERE, null, ex);
                logger.error(ex.getMessage());
                to.setConverterFlg(false);
                to.setMessage(ex.getMessage());
                FileUtil.makefile2(to.getObjectName(), conBeforeText);
            }
        }
        
        for(ProcedureInfoTO to : functions) {
            logger.info("-------------functions:" + to.getObjectName() + "-------------start");
            if(to.isBuildError()) {
                functionBuildErrorCount++;
                continue;
            }
            String conBeforeText = "";
            try {
                conBeforeText = CommonUtil.handBeforConverter(to.getObjectText());
                String outSql = convertMain.convert(conBeforeText);
                if(outSql != null) {
                    String text = managerConverterOut(outSql, to.getObjectName());
                    
                    text = handPackageType(text, packageTypeMap);
                    
                    text = handSpecialText(text);
                    to.setConverterText(text);
                    to.setConverterFlg(true);
                    functionSuccessCount++;
                } else {
                    to.setConverterFlg(false);
                    FileUtil.makefile2(to.getObjectName(), conBeforeText);
                }
            } catch(Exception ex) {
                //Logger.getLogger(Plsqlconverter.class.getName()).log(Level.SEVERE, null, ex);
                logger.error(ex.getMessage());
                to.setConverterFlg(false);
                to.setMessage(ex.getMessage());
                FileUtil.makefile2(to.getObjectName(), conBeforeText);
            }
        }
        
        for(ProcedureInfoTO to : views){
            logger.info("-------------view:" + to.getObjectName() + "-------------start");
            String outSql = "CREATE OR REPLACE FUNCTION func_ctp_encodeText\n" +
                "RETURN VARCHAR2 IS\n" +
                "BEGIN\n" +
                to.getObjectText() + ";" +
                "RETURN '';\n" +
                "END func_ctp_encodeText;";
            if(to.isBuildError()) {
                viewBuildErrorCount++;
                continue;
            }
            try{
                outSql = CommonUtil.handBeforConverter(outSql);
                outSql = convertMain.convert(outSql);
                
                if(outSql != null){
                    outSql = handSpecialText(outSql);
                    Pattern pattern = Pattern.compile("begin\\s*(perform)*\\s*\\(([\\s\\S]*)\\);",Pattern.CASE_INSENSITIVE);
                    Matcher matcher = pattern.matcher(outSql);
                    if(matcher.find()){
                        String perform = matcher.group(1);
                        if("perform".equalsIgnoreCase(perform)) {
                            outSql = matcher.group(2);
                        } else {
                            outSql = "(" + matcher.group(2) + ")";
                        }
                    } else {
                    	outSql = "";
                    }
                    outSql = "CREATE OR REPLACE VIEW " + to.getObjectOwner() + "." + to.getObjectName() + " AS \n" + outSql.trim() + ";";
                    to.setConverterText(outSql);
                    to.setConverterFlg(true);
                    viewSuccessCount++;
                } else {
                    to.setConverterFlg(false);
                }
            }catch(Exception ex){
                to.setConverterFlg(false);
                to.setMessage(ex.getMessage());
            }
        }
    }
    
    private String handPackageType(String text, Map<String, Map<String, String>> packageTypeMap){
        
        Pattern patternbegin = Pattern.compile("\\s+begin\\s+", Pattern.CASE_INSENSITIVE);
        Matcher matcherbegin = patternbegin.matcher(text);
        String defineText = "";
        String bodyText = "";
        if(matcherbegin.find()) {
            defineText = text.substring(0, matcherbegin.start());
            bodyText = text.substring(matcherbegin.start());
        } else {
            defineText = text;
            bodyText = "";
        }
        
        Pattern patterndeclare = Pattern.compile("\\s+declare\\s+", Pattern.CASE_INSENSITIVE);
        Matcher matcherdeclare = patterndeclare.matcher(defineText);
        String functiondefineText = "";
        String declareText = "";
        if(matcherdeclare.find()) {
                functiondefineText = defineText.substring(0, matcherdeclare.start());
                declareText = defineText.substring(matcherdeclare.start());
            } else {
                functiondefineText = defineText;
                declareText = "";
        }
        
        for(String packageName :packageTypeMap.keySet()){
            Map<String, String> typeMap = packageTypeMap.get(packageName);
            for(String key: typeMap.keySet()) {
                String typeName = packageName + "." + key;
                String typeConverterName = typeMap.get(key);
                
                functiondefineText = conterverTypeInFunctionDefine(functiondefineText, typeName, typeConverterName);
                declareText = conterverTypeInDeclare(declareText, typeName, typeConverterName);
            }
        }
        
        return functiondefineText + declareText + bodyText;
    }
    
    /* 
    * Handling special characters
    */
     private String handSpecialText(String text){
        text = CommonUtil.replaceText(text, "sys_refcursor", "refcursor");
        
        text = CommonUtil.replaceText(text, "ref_cursor", "refcursor");
        
        text = CommonUtil.replaceText(text, "strict", "");
        
        //text = repleceOpenForSQL(text);
        
        //text = CommonUtil.repalceTable(text);
        
        //text = CommonUtil.insertIntoSqladdas(text);
        
        text = CommonUtil.convertDollarSign(text);
        
        text = changRecordAliasing(text);
        
        return text;
     }
     
     private String changRecordAliasing(String text) {
        Pattern patternbegin = Pattern.compile("\\s+begin\\s+", Pattern.CASE_INSENSITIVE);
        Matcher matcherbegin = patternbegin.matcher(text);
        String defineText = "";
        String bodyText = "";
        if(matcherbegin.find()) {
            defineText = text.substring(0, matcherbegin.start());
            bodyText = text.substring(matcherbegin.start());
        } else {
            defineText = text;
            bodyText = "";
        }
        
        Pattern pattern = Pattern.compile("\\s+type\\s+(\\w+)\\s+is\\s+([\\w\\s]+)([\\S\\s]*?);", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(defineText);
        
        StringBuffer defineStrBuffer = new StringBuffer();
        Map<String, String> typeMap = new HashMap<>();
        int start = 0;
        while (matcher.find()) {
            typeMap.put(matcher.group(1), CommonUtil.typeOracle2Pg(matcher.group(2).trim()));
            defineStrBuffer.append(defineText.substring(start, matcher.start()));
            start = matcher.end();
        }
        defineStrBuffer.append(defineText.substring(start));
        
        defineText = defineStrBuffer.toString();
        for(String type : typeMap.keySet()) {
            String pgType = typeMap.get(type);
            
            Pattern patternType2 = Pattern.compile("\\s+\\w+\\s+"+ type + "(([\\s\\S])*?;)", Pattern.CASE_INSENSITIVE);
            Matcher matcherType2 = patternType2.matcher(defineText);
            start = 0;
            StringBuffer textbuffer = new StringBuffer();
            while(matcherType2.find()) {
                String endValue = matcherType2.group(1);
                textbuffer.append(defineText.substring(start, matcherType2.end() - type.length() - endValue.length()));
                textbuffer.append(pgType);
                textbuffer.append(defineText.substring(matcherType2.end() - endValue.length(), matcherType2.end()));
                start = matcherType2.end();
            }
            textbuffer.append(defineText.substring(start));
            defineText = textbuffer.toString();
        }
        
        return defineText + bodyText;
     }
     
     /*
     open o_result for sql => open o_result for execute sql  
     */
//     private String repleceOpenForSQL(String text){
//         Pattern patternType = Pattern.compile("[^\\w+]open\\s+\\w+\\s+for(\\s+\\w+\\s*;)", Pattern.CASE_INSENSITIVE);
//        Matcher matcherType = patternType.matcher(text);
//        StringBuffer temp = new StringBuffer();
//        int start = 0;
//        int end = 0;
//        while(matcherType.find()) {
//            String endValue = matcherType.group(matcherType.groupCount());
//            temp.append(text.substring(start, matcherType.end() - endValue.length()));
//            temp.append(" execute");
//            temp.append(text.substring(matcherType.end() - endValue.length(), matcherType.end()));
//            start = matcherType.end();
//            end = matcherType.end();
//        }
//        if(start != 0) {
//            temp.append(text.substring(end));
//            text = temp.toString();
//        }
//        return text;
//     }
     

    /*
    *   Handling custom types in packagea
    *   *type Cursor_Ref is ref cursor;
    *   *procedure GetBizObject(p_ReCursor0 out Cursor_Ref);=>procedure GetBizObject(p_ReCursor0 out refcursor);
    */
    private String managerPackageSpecialText(String packageBodyText, String packateName, Map<String, Map<String, String>> packageTypeMap){
        Pattern patternFunction = Pattern.compile("create\\s+or\\s+replace\\s+function\\s+" + packateName + "\\.\\w+", Pattern.CASE_INSENSITIVE);
        Matcher matcherFunction = patternFunction.matcher(packageBodyText);
        int functionCount = 0;
        int functionStartIndex = 0;
        List<String> functionList = new ArrayList<>();
        while(matcherFunction.find()) {
            functionCount++;
            if(functionCount == 1) {
                continue;
            }
            
            String strFunctionText = packageBodyText.substring(functionStartIndex, matcherFunction.start());
            functionStartIndex = matcherFunction.start();
            functionList.add(strFunctionText);
        }
        functionList.add(packageBodyText.substring(functionStartIndex));
        
        StringBuffer rntText = new StringBuffer();
        for(int i = 0; i < functionList.size(); i++) {
            String functionText = functionList.get(i);
            
            Pattern patternbegin = Pattern.compile("\\s+begin\\s+", Pattern.CASE_INSENSITIVE);
            Matcher matcherbegin = patternbegin.matcher(functionText);
            String defineText = "";
            String bodyText = "";
            if(matcherbegin.find()) {
                defineText = functionText.substring(0, matcherbegin.start());
                bodyText = functionText.substring(matcherbegin.start());
            } else {
                defineText = functionText;
                bodyText = "";
            }
            
            Pattern patterndeclare = Pattern.compile("\\s+declare\\s+", Pattern.CASE_INSENSITIVE);
            Matcher matcherdeclare = patterndeclare.matcher(defineText);
            String functiondefineText = "";
            String declareText = "";
            if(matcherdeclare.find()) {
                functiondefineText = defineText.substring(0, matcherdeclare.start());
                declareText = defineText.substring(matcherdeclare.start());
            } else {
                functiondefineText = defineText;
                declareText = "";
            }
            
            
            for(String key :packageTypeMap.keySet()) {
                Map<String,String> typeMap = packageTypeMap.get(key);
                for(String type : typeMap.keySet()) {

                    String pgType = typeMap.get(type);
                    
                    if(!key.equals(packateName)) {
                        type = key + "." + type;
                    }
                    functiondefineText = conterverTypeInFunctionDefine(functiondefineText, type, pgType);
                    declareText = conterverTypeInDeclare(declareText, type, pgType);
                }
            }

            rntText.append(functiondefineText + declareText + bodyText);
        }
        return rntText.toString();
    }
    
    private String conterverTypeInFunctionDefine(String defineText,String type, String pgType){
        Pattern patternType = Pattern.compile("[^\\w](\\w+)\\s+((OUT)|(IN)|(IN\\s+OUT))?\\s+"+ type +"(\\s*[^\\w])", Pattern.CASE_INSENSITIVE);
        Matcher matcherType = patternType.matcher(defineText);
        StringBuffer text = new StringBuffer();
        int start = 0;
        int end = 0;
        while(matcherType.find()) {
            String endValue = matcherType.group(matcherType.groupCount());
            text.append(defineText.substring(start, matcherType.end() - type.length() - endValue.length()));
            text.append(pgType);
            text.append(defineText.substring(matcherType.end() - endValue.length(), matcherType.end()));
            start = matcherType.end();
            end = matcherType.end();
        }

        if(start != 0) {
            text.append(defineText.substring(end));
            defineText = text.toString();
        }
        patternType = Pattern.compile("\\s+returns\\s+"+ type +"(\\s+)", Pattern.CASE_INSENSITIVE);
        matcherType = patternType.matcher(defineText);
        text = new StringBuffer();
        start = 0;
        end = 0;
        while(matcherType.find()) {
            String endValue = matcherType.group(matcherType.groupCount());
            text.append(defineText.substring(start, matcherType.end() - type.length() - endValue.length()));
            text.append(pgType);
            text.append(defineText.substring(matcherType.end() - endValue.length(), matcherType.end()));
            start = matcherType.end();
            end = matcherType.end();
        }

        if(start != 0) {
            text.append(defineText.substring(end));
            defineText = text.toString();
        }
        
        return defineText;
    }
    
    private String conterverTypeInDeclare(String defineText,String type, String pgType){
        //String pgType = typeMap.get(type);
        

        //Pattern patternType2 = Pattern.compile("\\s+\\w+\\s+"+ type + "(\\s*(\\/\\*[\\s\\S]*\\*\\/\\s*)*;)", Pattern.CASE_INSENSITIVE);
        Pattern patternType2 = Pattern.compile("\\s+\\w+\\s+"+ type + "(([\\s\\S])*?;)", Pattern.CASE_INSENSITIVE);
        Matcher matcherType2 = patternType2.matcher(defineText);
        StringBuffer text = new StringBuffer();;
        int start = 0;
        int end = 0;
        while(matcherType2.find()) {
            String endValue = matcherType2.group(1);
            text.append(defineText.substring(start, matcherType2.end() - type.length() - endValue.length()));
            text.append(pgType);
            text.append(defineText.substring(matcherType2.end() - endValue.length(), matcherType2.end()));
            start = matcherType2.end();
            end = matcherType2.end();
        }

        if(start != 0) {
            text.append(defineText.substring(end));
            defineText = text.toString();
        }
        return defineText;
    }
   
    /**
     * Handling end function
     * *end+functionName; =》 end;
     */
    private String managerConverterOut(String out, String name) {
        String stRreplace = "(?i)end " + name;
        String rnt  = out.replaceAll(stRreplace, "end");
        return rnt;
    }

    /**
     * Get ALL procedures
     */
    private boolean getProcedure(){
        DefaultTableModel model = (DefaultTableModel)tblProcedureList.getModel();
            
        model.setRowCount(0);
        Connection connection = null;
        try {
            ConnInfoTO conninfo = new ConnInfoTO(txtSourceIp.getText().trim(),
                              Integer.parseInt(txtSourcePort.getValue().toString()),
                              txtSourceUserName.getText().trim(),
                              String.valueOf(txtSourcePassword.getPassword()),
                              txtSourceServerName.getText().trim(), null);

            connection = JdbcHelper.getConnection(DBType.ORACLE, conninfo);
            
            schemaName = getSchemaName(connection);
            
            packageDelare = getProducers(connection, OBJECT_TYPE_PACKAGE);

            procedures = getProducers(connection, OBJECT_TYPE_PROCEDURE);
            functions = getProducers(connection, OBJECT_TYPE_FUNCTION);
            packages = getProducers(connection, OBJECT_TYPE_PACKAGE_BODY);
            views = getViews(connection, schemaName);
            
            for(ProcedureInfoTO to:packageDelare) {
                String text = getProcedureText(connection, OBJECT_TYPE_PACKAGE, to.getObjectName(), to.getObjectOwner());
                to.setObjectText(text);
            }
            
            if(procedures != null) {
                for(ProcedureInfoTO to : procedures) {
                     model.addRow(to.getShow());
                     to.setObjectText(getProcedureText(connection, to.getObjectType(), to.getObjectName(), to.getObjectOwner()));
                }
            }
            
            if(functions != null) {
                for(ProcedureInfoTO to : functions) {
                     model.addRow(to.getShow());
                     to.setObjectText(getProcedureText(connection, to.getObjectType(), to.getObjectName(), to.getObjectOwner()));
                }
            }
            
            if(packages != null) {
                for(ProcedureInfoTO to : packages) {
                     model.addRow(to.getShow());
                     to.setObjectText(getProcedureText(connection, to.getObjectType(), to.getObjectName(), to.getObjectOwner()));
                     //to.setPackageText(getProcedureText(connection, OBJECT_TYPE_PACKAGE, to.getObjectName(), to.getObjectOwner()));
                }
            }
            
            if(views != null){
                for(ProcedureInfoTO to : views) {
                     model.addRow(to.getShow());

                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorTitle"), JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            if(connection != null) {
                JdbcHelper.close(connection);
            }
        }
        
        return true;
    }
    
    /*
    * get view
    */
    private List<ProcedureInfoTO> getViews(Connection connection, String userName){
        PreparedStatement statement = null;
        try {
            //String sql ="SELECT VIEW_NAME,TEXT FROM ALL_VIEWS WHERE OWNER = ?";
            String sql =    "SELECT v.VIEW_NAME,v.TEXT,o.STATUS\n" +
                            "FROM SYS.ALL_VIEWS v \n" +
                            "LEFT JOIN SYS.ALL_Objects o \n" +
                            "ON v.OWNER = o.OWNER AND v.VIEW_NAME = o.OBJECT_NAME AND o.OBJECT_TYPE = 'VIEW'\n" +
                            "WHERE v.OWNER = ? "
                            //+ " and v.VIEW_NAME = 'ADP_VW_BIZOBJ_PRINT' "
                            + " order by v.VIEW_NAME ";
            statement = connection.prepareStatement(sql);
            statement.setString(1,userName);
            ResultSet result = statement.executeQuery();
            List<ProcedureInfoTO> list = new ArrayList<>();
            while(result.next()) {
                String viewName = result.getString("VIEW_NAME");
                ProcedureInfoTO procedure = new ProcedureInfoTO(viewName,OBJECT_TYPE_VIEW,null,userName);
                procedure.setObjectText(result.getString("TEXT"));
                    procedure.setBuildError(result.getString("STATUS").equals("INVALID"));
                list.add(procedure);
            }
            return list;
        } catch (Exception ex) {
            //Logger.getLogger(Plsqlconverter.class.getName()).log(Level.SEVERE, null, ex);
            logger.error(ex.getMessage());
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    //Logger.getLogger(Plsqlconverter.class.getName()).log(Level.SEVERE, null, ex);
                    logger.error(ex.getMessage());
                }
            }
        }
        return null;
    }
    
    /*
    * Get DDL
    */
    private String getProcedureText(Connection connection, String type, String name,String ower) throws SQLException{
        String sql = "SELECT "
                + " TEXT "
                + "FROM "
                + " SYS.ALL_SOURCE "
                + "WHERE "
                + " TYPE= ? "
                + " AND OWNER= (select USER FROM DUAL) "
                + " AND NAME=? "
                + "ORDER BY LINE";
        
        try(PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, type);
            statement.setString(2, name);
            ResultSet executeQuery = statement.executeQuery();
            StringBuilder source = null;
            
            while(executeQuery.next()) {
                final String line = executeQuery.getString(1);
                if (source == null) {
                    source = new StringBuilder(200);
                }
                source.append(line);
            }
            return  insertCreateReplace(type, source.toString(), ower);
        }
    }
    
    /*
    * add [create or replace] to DDL
    */
    private String insertCreateReplace(String type, String source, String owner){
        Pattern srcPattern = Pattern.compile("^(" + type + ")\\s+(\"{0,1}\\w+\"{0,1})", Pattern.CASE_INSENSITIVE);
        Matcher matcher = srcPattern.matcher(source);
        if (matcher.find()) {
            return
                "CREATE OR REPLACE " + matcher.group(1) + " " +
                owner + "." + matcher.group(2) +
                source.substring(matcher.end());
        }
        return source;
    }
    
    /**
     * get schema Name
     */
    private String getSchemaName(Connection connection){
        String rnt = null;
        String sql = "select USER FROM DUAL";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet executeQuery = statement.executeQuery();
            
            if(executeQuery.next() ) {
                rnt = executeQuery.getString(1);
            } else {
               
            }
        } catch (Exception ex) {
            
        }
        return rnt;
    }
    
    /**
     * Get Procedures by type
     */
    private List<ProcedureInfoTO> getProducers(Connection connection, String objectType){
        
        PreparedStatement statement = null;
        try {

            String sql = "select "
                    + " o.* "
                    + "from SYS.ALL_OBJECTS o "
                    + "WHERE o.OBJECT_TYPE IN ('" + objectType  +  "') AND o.OWNER=(select USER FROM DUAL) "
                    //+ " and OBJECT_NAME in ('LEFT') "
                    + "ORDER BY o.OBJECT_NAME";
            statement = connection.prepareStatement(sql);
            
            ResultSet executeQuery = statement.executeQuery();
            List<ProcedureInfoTO> list = new ArrayList<>();
            
            while(executeQuery.next()) {
                String name = executeQuery.getString("OBJECT_NAME");
                String id = executeQuery.getString("OBJECT_ID");
                String type = executeQuery.getString("OBJECT_TYPE");
                String owner = executeQuery.getString("OWNER");
                String status = executeQuery.getString("STATUS");
                
                ProcedureInfoTO procedure = new ProcedureInfoTO(name, type, id, owner);
                procedure.setBuildError(!"VALID".equals(status));
                list.add(procedure);
                
            }
            
            return list;
        } catch (Exception ex) {
            //Logger.getLogger(Plsqlconverter.class.getName()).log(Level.SEVERE, null, ex);
            logger.error(ex.getMessage());
            return null;
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    //Logger.getLogger(Plsqlconverter.class.getName()).log(Level.SEVERE, null, ex);
                    logger.error(ex.getMessage());
                }
            }
        }
    }
    
    /**
     * init
     */
    private void customView(){
        treeNavigation.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) treeNavigation.getCellRenderer();
        renderer.setLeafIcon(null);
        renderer.setClosedIcon(null);
        renderer.setOpenIcon(null);
        treeNavigation.setCellRenderer(renderer);
        
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("");
        DefaultMutableTreeNode branchNode;
        branchNode = new DefaultMutableTreeNode(constBundle.getString("setdbproperties"));
        rootNode.add(branchNode);
        branchNode = new DefaultMutableTreeNode(constBundle.getString("getSourceProcuder"));
        rootNode.add(branchNode);
        branchNode = new DefaultMutableTreeNode(constBundle.getString("converter"));
        rootNode.add(branchNode);
        
        branchNode = new DefaultMutableTreeNode(constBundle.getString("createFunction"));
        rootNode.add(branchNode);
        //branchNode = new DefaultMutableTreeNode(constBundle.getString("finish"));
        //rootNode.add(branchNode);
        
        treeNavigation.setModel(new DefaultTreeModel(rootNode));
        treeNavigation.setSelectionRow(0);
        
        card = new CardLayout();
        pnlMain.setLayout(card);
        pnlMain.add(jpnTargetInfo, constBundle.getString("setdbproperties"));
        pnlMain.add(jpnSourceProducerInfo, constBundle.getString("getSourceProcuder"));
        pnlMain.add(jpnRresult, constBundle.getString("converter"));
        pnlMain.add(jpnCreateFunctionResult, constBundle.getString("createFunction"));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpnTargetInfo = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtSourceIp = new javax.swing.JTextField();
        txtSourceServerName = new javax.swing.JTextField();
        txtSourceUserName = new javax.swing.JTextField();
        txtSourcePort = new javax.swing.JSpinner();
        btnSourceTestConnection = new javax.swing.JButton();
        txtSourcePassword = new javax.swing.JPasswordField();
        //txtCreatTableSQLPath = new javax.swing.JTextField();
        //btnSelectSqlFile = new javax.swing.JButton();
        jpnSourceProducerInfo = new javax.swing.JPanel();
        pnlProcedure = new javax.swing.JScrollPane();
        tblProcedureList = new javax.swing.JTable();
        jpnRresult = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblResult = new javax.swing.JTable();
        jpnCreateFunctionResult = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblCreateFunctionResult = new javax.swing.JTable();
        dagCheck = new javax.swing.JDialog();
        jPanel3 = new javax.swing.JPanel();
        //chxFatalErrors = new javax.swing.JCheckBox();
        chxOtherWarnings = new javax.swing.JCheckBox();
        chxExtraWarnings = new javax.swing.JCheckBox();
        chxPerformanceWarnings = new javax.swing.JCheckBox();
        btnCheckExecute = new javax.swing.JButton();
        btnCheckCancel = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        treeNavigation = new javax.swing.JTree();
        btnBackup = new javax.swing.JButton();
        btnNext = new javax.swing.JButton();
        brnFininsh = new javax.swing.JButton();
        pnlMain = new javax.swing.JPanel();
        btnPgCheck = new javax.swing.JButton();

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, constBundle.getString("sourcetitle"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP));

        jLabel1.setText(constBundle.getString("IP"));

        jLabel2.setText(constBundle.getString("port"));

        jLabel3.setText(constBundle.getString("sername"));

        jLabel4.setText(constBundle.getString("username"));

        jLabel10.setText(constBundle.getString("password"));

        txtSourcePort.setValue(1521);

        btnSourceTestConnection.setText(constBundle.getString("testConnection"));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(txtSourceIp))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(txtSourcePort, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 185, Short.MAX_VALUE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(txtSourceServerName))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtSourcePassword)
                                    .addComponent(txtSourceUserName)))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSourceTestConnection)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtSourceIp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtSourcePort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSourceServerName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSourceUserName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtSourcePassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSourceTestConnection)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        //btnSelectSqlFile.setText(constBundle.getString("selectFile"));

        javax.swing.GroupLayout jpnTargetInfoLayout = new javax.swing.GroupLayout(jpnTargetInfo);
        jpnTargetInfo.setLayout(jpnTargetInfoLayout);
        jpnTargetInfoLayout.setHorizontalGroup(
            jpnTargetInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnTargetInfoLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jpnTargetInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jpnTargetInfoLayout.createSequentialGroup()
                        //.addComponent(txtCreatTableSQLPath, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE)
                        //.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        //.addComponent(btnSelectSqlFile)
                    		))
                .addContainerGap(32, Short.MAX_VALUE))
        );
        jpnTargetInfoLayout.setVerticalGroup(
            jpnTargetInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnTargetInfoLayout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(jpnTargetInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    //.addComponent(txtCreatTableSQLPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    //.addComponent(btnSelectSqlFile)
                		)
                .addContainerGap(43, Short.MAX_VALUE))
        );

        tblProcedureList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "名称", "类型"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblProcedureList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblProcedureList.setShowHorizontalLines(false);
        pnlProcedure.setViewportView(tblProcedureList);

        javax.swing.GroupLayout jpnSourceProducerInfoLayout = new javax.swing.GroupLayout(jpnSourceProducerInfo);
        jpnSourceProducerInfo.setLayout(jpnSourceProducerInfoLayout);
        jpnSourceProducerInfoLayout.setHorizontalGroup(
            jpnSourceProducerInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnSourceProducerInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlProcedure, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jpnSourceProducerInfoLayout.setVerticalGroup(
            jpnSourceProducerInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnSourceProducerInfoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnlProcedure, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tblResult.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "", "", "", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tblResult);
        if (tblResult.getColumnModel().getColumnCount() > 0) {
            tblResult.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("typeTitle"));
            tblResult.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("successCountTitle"));
            tblResult.getColumnModel().getColumn(2).setHeaderValue(constBundle.getString("failCountTitle"));
            tblResult.getColumnModel().getColumn(3).setHeaderValue(constBundle.getString("countBuildCount"));
            tblResult.getColumnModel().getColumn(4).setHeaderValue(constBundle.getString("countTitle"));
        }

        javax.swing.GroupLayout jpnRresultLayout = new javax.swing.GroupLayout(jpnRresult);
        jpnRresult.setLayout(jpnRresultLayout);
        jpnRresultLayout.setHorizontalGroup(
            jpnRresultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnRresultLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 449, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpnRresultLayout.setVerticalGroup(
            jpnRresultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnRresultLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE)
                .addContainerGap())
        );

        tblCreateFunctionResult.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "", "", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(tblCreateFunctionResult);
        if (tblCreateFunctionResult.getColumnModel().getColumnCount() > 0) {
            tblCreateFunctionResult.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("typeTitle"));
            tblCreateFunctionResult.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("successCountTitle"));
            tblCreateFunctionResult.getColumnModel().getColumn(2).setHeaderValue(constBundle.getString("failCountTitle"));
            tblCreateFunctionResult.getColumnModel().getColumn(3).setHeaderValue(constBundle.getString("countTitle"));
        }

        javax.swing.GroupLayout jpnCreateFunctionResultLayout = new javax.swing.GroupLayout(jpnCreateFunctionResult);
        jpnCreateFunctionResult.setLayout(jpnCreateFunctionResultLayout);
        jpnCreateFunctionResultLayout.setHorizontalGroup(
            jpnCreateFunctionResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnCreateFunctionResultLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 449, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpnCreateFunctionResultLayout.setVerticalGroup(
            jpnCreateFunctionResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnCreateFunctionResultLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE)
                .addContainerGap())
        );

        dagCheck.setTitle(constBundle.getString("checkTitle"));
        dagCheck.setAlwaysOnTop(true);
        dagCheck.setLocation(new java.awt.Point(0, 0));
        dagCheck.setModal(true);
        dagCheck.setSize(new java.awt.Dimension(300, 250));

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, constBundle.getString("checkTitle"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP));

        //chxFatalErrors.setText(constBundle.getString("checkfatalerror"));

        chxOtherWarnings.setText(constBundle.getString("checkotherwarnings"));

        chxExtraWarnings.setText(constBundle.getString("checkextrawarnings"));

        chxPerformanceWarnings.setText(constBundle.getString("checkperformancewarnings"));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chxPerformanceWarnings)
                    .addComponent(chxExtraWarnings)
                    .addComponent(chxOtherWarnings)
                    //.addComponent(chxFatalErrors)
                    )
                .addContainerGap(95, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                //.addComponent(chxFatalErrors)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chxOtherWarnings)
                .addGap(7, 7, 7)
                .addComponent(chxExtraWarnings)
                .addGap(10, 10, 10)
                .addComponent(chxPerformanceWarnings)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnCheckExecute.setText(constBundle.getString("checkExecute"));

        btnCheckCancel.setText(constBundle.getString("checkCancel"));

        javax.swing.GroupLayout dagCheckLayout = new javax.swing.GroupLayout(dagCheck.getContentPane());
        dagCheck.getContentPane().setLayout(dagCheckLayout);
        dagCheckLayout.setHorizontalGroup(
            dagCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dagCheckLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dagCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dagCheckLayout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(btnCheckExecute)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCheckCancel))
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        dagCheckLayout.setVerticalGroup(
            dagCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dagCheckLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dagCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCheckExecute)
                    .addComponent(btnCheckCancel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        treeNavigation.setBorder(javax.swing.BorderFactory.createTitledBorder(null, constBundle.getString("navigation"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP));
        treeNavigation.setModel(null);
        treeNavigation.setEnabled(false);
        treeNavigation.setRootVisible(false);
        jScrollPane1.setViewportView(treeNavigation);

        btnBackup.setText(constBundle.getString("backup"));

        btnNext.setText(constBundle.getString("next"));

        brnFininsh.setText(constBundle.getString("finish"));

        javax.swing.GroupLayout pnlMainLayout = new javax.swing.GroupLayout(pnlMain);
        pnlMain.setLayout(pnlMainLayout);
        pnlMainLayout.setHorizontalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        pnlMainLayout.setVerticalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 430, Short.MAX_VALUE)
        );

        btnPgCheck.setText(constBundle.getString("pgChexk"));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 297, Short.MAX_VALUE)
                        .addComponent(btnBackup)
                        .addGap(58, 58, 58)
                        .addComponent(btnNext)
                        .addGap(47, 47, 47)
                        .addComponent(brnFininsh)
                        .addGap(50, 50, 50)
                        .addComponent(btnPgCheck)
                        .addGap(54, 54, 54))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
                    .addComponent(pnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNext)
                    .addComponent(brnFininsh)
                    .addComponent(btnBackup)
                    .addComponent(btnPgCheck))
                .addGap(31, 31, 31))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Plsqlconverter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Plsqlconverter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Plsqlconverter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Plsqlconverter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Plsqlconverter().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton brnFininsh;
    private javax.swing.JButton btnBackup;
    private javax.swing.JButton btnCheckCancel;
    private javax.swing.JButton btnCheckExecute;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnPgCheck;
    //private javax.swing.JButton btnSelectSqlFile;
    private javax.swing.JButton btnSourceTestConnection;
    private javax.swing.JCheckBox chxExtraWarnings;
    //private javax.swing.JCheckBox chxFatalErrors;
    private javax.swing.JCheckBox chxOtherWarnings;
    private javax.swing.JCheckBox chxPerformanceWarnings;
    private javax.swing.JDialog dagCheck;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JPanel jpnCreateFunctionResult;
    private javax.swing.JPanel jpnRresult;
    private javax.swing.JPanel jpnSourceProducerInfo;
    private javax.swing.JPanel jpnTargetInfo;
    private javax.swing.JPanel pnlMain;
    private javax.swing.JScrollPane pnlProcedure;
    private javax.swing.JTable tblCreateFunctionResult;
    private javax.swing.JTable tblProcedureList;
    private javax.swing.JTable tblResult;
    private javax.swing.JTree treeNavigation;
    //private javax.swing.JTextField txtCreatTableSQLPath;
    private javax.swing.JTextField txtSourceIp;
    private javax.swing.JPasswordField txtSourcePassword;
    private javax.swing.JSpinner txtSourcePort;
    private javax.swing.JTextField txtSourceServerName;
    private javax.swing.JTextField txtSourceUserName;
    // End of variables declaration//GEN-END:variables
    
}
