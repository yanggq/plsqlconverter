/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.highgo.plsqlconverter.util;

import com.highgo.plsqlconverter.model.ProcedureInfoTO;
import com.highgo.plsqlconverter.view.Plsqlconverter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author yanggq
 */
public class FileUtil {
    
    public static final String FILE_SUFFIX_NAME = ".sql";
    public static final String SEPARATE_LINE = "-------------------------------------------------------------------------------";
    
    public static void makefile2(String fileName, String text) {
         File succesDdir = new File("converterfile" + File.separator + "fail" +  File.separator + "converbefore");
        if(!succesDdir.exists()) {
            succesDdir.mkdirs();
        }
         File sqlfile = new File(succesDdir, fileName + FILE_SUFFIX_NAME);
         
         try (BufferedWriter bw = new BufferedWriter(new FileWriter(sqlfile))){
            bw.write(text);
            bw.flush();

        } catch (Exception ex) {
            Logger.getLogger(FileUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void makeFile(List<ProcedureInfoTO> list){
        
        File succesDdir = new File("converterfile" + File.separator + "success");
        if(!succesDdir.exists()) {
            succesDdir.mkdirs();
        }
        
        File faiDdir = new File("converterfile" + File.separator + "fail");
        if(!faiDdir.exists()) {
            faiDdir.mkdirs();
        }
        
        File buildErrorDdir = new File("converterfile" + File.separator + "notconverter");
        if(!buildErrorDdir.exists()) {
            buildErrorDdir.mkdirs();
        }
        
        for(ProcedureInfoTO to : list) {
            File sqlfile = null;
            if(to.isConverterFlg()) {
                sqlfile = new File(succesDdir, to.getObjectName() + FILE_SUFFIX_NAME);
            } else {
                if( to.isBuildError() ){
                    sqlfile = new File(buildErrorDdir, to.getObjectName() + FILE_SUFFIX_NAME);
                } else {
                    sqlfile = new File(faiDdir, to.getObjectName() + FILE_SUFFIX_NAME);
                }
            }
            
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(sqlfile))){
                if(Plsqlconverter.OBJECT_TYPE_VIEW.equals(to.getObjectType())){
                    String objectText = "CREATE OR REPLACE VIEW " + to.getObjectOwner() + "." + to.getObjectName() + " AS \n" + to.getObjectText().trim() + ";";
                    bw.write(objectText);
                }else{
                    bw.write(to.getObjectText());
                }
                if(to.isConverterFlg()) {
                    bw.newLine();
                    bw.write(SEPARATE_LINE);
                    bw.newLine();
                    bw.write(to.getConverterText());
                }
                bw.flush();
               
            } catch (Exception ex) {
                Logger.getLogger(FileUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void makeFileAll(List<ProcedureInfoTO> procedures, List<ProcedureInfoTO> functions, List<ProcedureInfoTO> packages, List<ProcedureInfoTO> views){
        
        File dir = new File("converterfile");
        if(!dir.exists()) {
            dir.mkdirs();
        }
        
        File sucessSqlfile = null;
        File failSqlfile = null;
        File buildErrorSqlfile = null;
        
        sucessSqlfile = new File(dir, "success" + FILE_SUFFIX_NAME);
        failSqlfile = new File(dir, "fail" + FILE_SUFFIX_NAME);
        buildErrorSqlfile = new File(dir, "notconverter" + FILE_SUFFIX_NAME);
        
        BufferedWriter successbw = null;
        BufferedWriter failbw = null;
        BufferedWriter buildErrorbw = null;
        
        try {
            successbw = new BufferedWriter(new FileWriter(sucessSqlfile));
            failbw = new BufferedWriter(new FileWriter(failSqlfile));
            buildErrorbw = new BufferedWriter(new FileWriter(buildErrorSqlfile));
            for(ProcedureInfoTO to : procedures) {
                if(to.isConverterFlg()) {
                    successbw.newLine();
                    successbw.write(to.getConverterText());
                } else {
                    if(to.isBuildError()) {
                        buildErrorbw.newLine();
                        buildErrorbw.write(to.getObjectText());
                    } else {
                        failbw.newLine();
                        failbw.write(to.getObjectText());
                    }
                }
            }
            
            for(ProcedureInfoTO to : functions) {
                if(to.isConverterFlg()) {
                    successbw.newLine();
                    successbw.write(to.getConverterText());
                } else {
                    if(to.isBuildError()) {
                        buildErrorbw.newLine();
                        buildErrorbw.write(to.getObjectText());
                    } else {
                        failbw.newLine();
                        failbw.write(to.getObjectText());
                    }
                }
            }
            for(ProcedureInfoTO to : views) {
                if(to.isConverterFlg()) {
                    successbw.newLine();
                    successbw.write(to.getConverterText());
                } else {
                    if(to.isBuildError()) {
                        buildErrorbw.newLine();
                        buildErrorbw.write(to.getObjectText());
                    } else {
                        failbw.newLine();
                        failbw.write(to.getObjectText());
                    }
                }
            }
            
            for(ProcedureInfoTO to : packages) {
                if(to.isConverterFlg()) {
                    successbw.newLine();
                    successbw.write(to.getConverterText());
                } else {
                    if(to.isBuildError()) {
                        buildErrorbw.newLine();
                        buildErrorbw.write(to.getObjectText());
                    } else {
                        failbw.newLine();
                        failbw.write(to.getObjectText());
                    }
                }
            }
            successbw.flush();
            failbw.flush();
            buildErrorbw.flush();
        } catch (Exception ex) {
            Logger.getLogger(FileUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(successbw != null) {
                try {
                    successbw.close();
                } catch (IOException ex) {
                    Logger.getLogger(FileUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            if(failbw != null) {
                try {
                    failbw.close();
                } catch (IOException ex) {
                    Logger.getLogger(FileUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            if(buildErrorbw != null) {
                try {
                    buildErrorbw.close();
                } catch (IOException ex) {
                    Logger.getLogger(FileUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public static void makeFile(String fileDir, String fileName, String text){
        File dir = new File("converterfile" + File.separator +fileDir);
        if(!dir.exists()) {
        	dir.mkdirs();
        }
        
        File sqlfile = new File(dir, fileName);
            
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(sqlfile))){
            
           bw.write(text);

           bw.flush();
           
        } catch (Exception ex) {
            Logger.getLogger(FileUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
