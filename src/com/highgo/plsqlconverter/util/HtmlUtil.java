package com.highgo.plsqlconverter.util;


import com.highgo.plsqlconverter.build.FunctionTO;
import com.highgo.plsqlconverter.model.ProcedureHtmlTO;
import com.highgo.plsqlconverter.model.ProcedureInfoTO;
import com.highgo.plsqlconverter.view.Plsqlconverter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class HtmlUtil {
	public static String TABLE_ID_SUCC = "_succ";
	
	public static String TABLE_ID_FAIL = "_fail";
        
        public static String TABLE_ID_NOCONVERTER = "_noconverter";


	public static void htmlInit(StringBuilder html) {
		html.append("<!DOCTYPE html>\r\n")
			.append("<html lang=\"en\">\r\n")
			.append("<meta charset=\"UTF-8\">\r\n")
			.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">\r\n");		
	}
	
	public static void setHead(StringBuilder html) {
		html.append("<head lang=\"en\">\r\n")
			.append("<link rel=\"stylesheet\" href=\"./assets/css/bootstrap.min.css\">\r\n")
			.append("<script src=\"./assets/js/jquery-3.3.1.min.js\"></script>\r\n")
			.append("<script src=\"./assets/js/jquery-ui.min.js\"></script>\r\n")
			.append("<script src=\"./assets/js/bootstrap.min.js\"></script>\r\n")
			.append("<style>\r\n")
			.append("tr th {\r\n")
			.append("	text-align: center;\r\n")
			.append("}\r\n")
			.append(".glyphicon:hover {\r\n")
			.append("	cursor: hand\r\n")
			.append("}\r\n")
			.append("</style>\r\n")
			.append("</head>\r\n");
	}
	
	public static StringBuilder sumTable(List<ProcedureHtmlTO> codes,StringBuilder html) {

		StringBuilder children = new StringBuilder();
		
		html.append("<div class=\"row\">\r\n")
			.append("	<div class=\"col-md-10\">\r\n")
			.append("		<div class=\"panel panel-default\">\r\n")
			.append("			<div class=\"panel-heading\">统计信息</div>\r\n");
		
		html.append("<table class=\"table\" style=\"text-align: center\">\r\n")
			.append("<thead>\r\n")
			.append("	<tr>\r\n")
			.append("	<td width='55px'>序号</td>\r\n") 
			.append("	<td width='150px'>类型</td>\r\n")
			.append("	<td width='100px'>成功数</td>\r\n")
			.append("	<td width='100px'>失败数</td>\r\n") 
                        .append("	<td width='100px'>未迁移数</td>\r\n") 
			.append("	<td width='100px'>合计</td>\r\n") 
			.append("<thead>\r\n");
			
		for(int i=0;i<codes.size();i++) {
			html.append("<tbody>\r\n")
			.append("	<tr>\r\n")
			.append("		<td>").append(i + 1).append("</td>\r\n")
			.append("		<td id='").append(codes.get(i).getTypeName()).append("'>").append(codes.get(i).getTypeName()).append("</td>\r\n") 
			.append("		<td name='cursor' onclick=test('").append(codes.get(i).getTypeName().replaceAll(" ", "")).append("',1)>").append(codes.get(i).getSuccessCount()).append("</td>\r\n")
			.append("		<td name='cursor' onclick=test('").append(codes.get(i).getTypeName().replaceAll(" ", "")).append("',2)>").append(codes.get(i).getFailCount()).append("</td>\r\n")
			.append("		<td name='cursor' onclick=test('").append(codes.get(i).getTypeName().replaceAll(" ", "")).append("',3)>").append(codes.get(i).getBuildErrorCount()).append("</td>\r\n")                                
			.append("		<td>").append(codes.get(i).getSuccessCount()+codes.get(i).getFailCount() + codes.get(i).getBuildErrorCount()).append("</td>\r\n") 
			.append("	</tr>\r\n")
			.append("</tbody>\r\n");
			
			//for children table
			childrenTable(codes.get(i), children);;
		}
		html.append("</table>\r\n");
		
		html.append("		</div>\r\n")
			.append("	</div>\r\n")
			.append("</div>\r\n");
		
		return children;
	}
	
	public static void childrenTable(ProcedureHtmlTO to,StringBuilder children) {
		
		//String table = "<table cellpadding='0' cellspacing='0' id='%s' name='content'>\r\n";
		int type = 1;
		String id = null;
		List<ProcedureInfoTO> infoList = null;
		String resultName = null;
		String title = "";
		while(type <= 3) {
			//set id value
			if(type == 1) {
				id = to.getTypeName().replaceAll(" ", "") + TABLE_ID_SUCC;
				infoList = to.getSuccessList();
				resultName = "成功";
				title = "转换成功信息";
			}else if(type == 2){
				id = to.getTypeName().replaceAll(" ", "") + TABLE_ID_FAIL;
				infoList = to.getFailList();
				resultName = "失败";
				title = "转换失败信息";
			} else {
                            id = to.getTypeName().replaceAll(" ", "") + TABLE_ID_NOCONVERTER;
                            infoList = to.getBuildErrorList();
                            resultName = "未迁移";
                            title = "未迁移信息";
                        }
			
			//Add thead 
			children.append("<div class=\"row\" id=\"" + id +  "\" name=\"content\">\r\n")
				.append("	<div class=\"col-md-12\">\r\n")
				.append("		<div class=\"panel panel-default\">\r\n")
				.append("<div class=\"panel-heading\">" + title  +  "</div>")
				.append("<table class=\"table\" style=\"text-align: center\">")
				.append("<thead>\r\n")
				.append("	<tr>\r\n");
			if(type == 2) {
				children.append("	<th></td>\r\n");
			}
			children.append("	<td width='55px'>序号</td>\r\n") 
				.append("	<td width='150px'>类型</td>\r\n")
				.append("	<td width='100px'>状态</td>\r\n")
				.append("	<td width='400px'>名称</td>\r\n");
			if(type == 2) {
				children.append("	<td width='400px'>信息</td>\r\n");
			}
			children.append("<thead>\r\n");
			
			//Add tbody
			for(int i=0;i<infoList.size();i++) {
				children.append("<tbody>\r\n")
				.append("	<tr>\r\n");
				if(type == 2) {
					children.append("<td><span class=\"glyphicon glyphicon-plus\" id=\"" + infoList.get(i).getObjectName() + i +"\" \r\n")
							.append("	style=\"cursor: pointer\"\r\n")
							.append("	onclick=\"openModal(this,'"+ infoList.get(i).getObjectName() + "')\" \r\n")
							.append("	xid=\"ERP_DRP_MATERIALSFAMILYIMORG\"></span></td>\r\n");
				}
				children.append("		<td>").append(i+ 1).append("</td>\r\n")
				.append("		<td>").append(infoList.get(i).getObjectType()).append("</td>\r\n") 
				.append("		<td>").append(resultName).append("</td>\r\n") 
				.append("		<td>").append(infoList.get(i).getObjectName()).append("</td>\r\n");
				if(type == 2) {
					children.append("		<td align=\"left\">").append(infoList.get(i).getMessage()).append("</td>\r\n");
				}
				children.append("	</tr>\r\n")
				.append("</tbody>\r\n");
			}
			children.append("</table>\r\n")
					.append("</div>")
					.append("</div>")
					.append("</div>");
			type ++;
		}
	}
	
	public static void setScript(StringBuilder html) {

		html.append("<script type=\"text/javascript\">\r\n")
			.append("$(document).ready(function(){\r\n")
			.append("	$(\"#modalDialog\").draggable({\r\n")
			.append("		handle : \".modal-header\", // 只能点击头部拖动\r\n")
			.append("		cursor : 'move',\r\n")
			.append("		refreshPositions : false,\r\n")
			.append("		scroll : false,\r\n")
			.append("		containment : [ -400, 0 ]//move position\r\n")
			.append("	});\r\n")
			.append("	$(\"#myModal\").css(\"overflow\", \"hidden\");\r\n")
			.append("	$('#myModal').modal({\r\n")
			.append("		show : false,\r\n")
			.append("		keyboard : false,\r\n")
			.append("		backdrop : false\r\n")
			.append("	})\r\n")
			.append("	$('#myModal').on('hide.bs.modal', function() {\r\n")
			.append("		var rowSpan = $('#rowSpan').val();\r\n")
			.append("		console.log(rowSpan)\r\n")
			.append("		$('#' + rowSpan).addClass('glyphicon-plus');//add +\r\n")
			.append("		$('#' + rowSpan).removeClass('glyphicon-minus');//remove -\r\n")
			.append("	});\r\n")
			.append("	$(\"div[name='content']\").hide();\r\n")	//hide All content
			.append("	$(\"td[name='cursor']\").hover(function(){\r\n")	//td hover
			.append("		$(this).css('color','red');$(this).css('cursor','pointer');\r\n")	//on
			.append("	},function(){\r\n")
			.append("		$(\"td[name='cursor']\").css('color','black');$(this).css('cursor','default');\r\n")	//off
			.append("	});\r\n")
			.append("});\r\n");
		html.append("function test(id,type){\r\n")//toggle function
			.append("	$(\"div[name='content']\").hide();\r\n")	//hide All content
			.append("	var obj = type == 1?id+'"+TABLE_ID_SUCC+"':type == 2?id+'"+TABLE_ID_FAIL+ "':id + '" + TABLE_ID_NOCONVERTER  + "';\r\n")
			.append("	console.log(obj);\r\n")
			.append("	$('#'+obj).show();\r\n")	//show select content
			.append("}\r\n");
			
		html.append("function openModal(obj, name) {\r\n")
			.append("	$('#myiframe').attr('src', './html/' + name + '.html');\r\n")
			.append("	$(obj).removeClass('glyphicon-plus');//remove +\r\n")
			.append("	$(obj).addClass('glyphicon-minus');//add -\r\n")
			.append("	var rowSpan = $(obj).attr('id');\r\n")
			.append("	$('#rowSpan').val(rowSpan);\r\n")
			.append("	$('#myModalLabel').html('Function ' + name);\r\n")
			.append("	$('#myModal').modal('show');\r\n")
			.append("}\r\n");
		
		html.append("</script>\r\n");
		
	}
	
	private static void htmlOpenDialog(StringBuilder html) {
		html.append("<input type=\"hidden\" id=\"rowSpan\" value=\"\" />\r\n")
		.append("<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\r\n")
		.append("	<div id=\"modalDialog\" class=\"modal-dialog modal-lg\" role=\"document\" style=\"width: 90%\">\r\n")
		.append("		<div class=\"modal-content\">\r\n")
		.append("			<div class=\"modal-header\">\r\n")
		.append("				<!--data-dismiss 监听关闭事件-->\r\n")
		.append("				<button type=\"button\" class=\"close\" data-dismiss=\"modal\"\r\n")
		.append("					aria-label=\"Close\">\r\n")
		.append("					<span aria-hidden=\"true\">&times;</span>\r\n")
		.append("				</button>\r\n")
		.append("				<h4 class=\"modal-title\" id=\"myModalLabel\">Function NAME1</h4>\r\n")
		.append("			</div>\r\n")
		.append("			<div class=\"modal-body\" style=\"height: 500px\">\r\n")
		.append("				<iframe id=\"myiframe\" width=\"100%\"\r\n")
		.append("					height=\"100%\" //规定 iframe的高度。\r\n")
		.append("					src=\"\"\r\n")
		.append("					frameborder=\"no\"//规定是否显示框架周围的边框。\r\n")
		.append("					marginwidth=\"0\" //定义 iframe 的左侧和右侧的边距。\r\n")
		.append("					marginheight=\"0\" //定义 iframe 的顶部和底部的边距。\r\n")
		.append("					scrolling=\"yes\" //规定是否在 iframe中显示滚动条。\r\n")
		.append("				>\r\n")
		.append("				</iframe>\r\n")
		.append("			</div>\r\n")
		.append("		</div>\r\n")
		.append("	</div>\r\n")
		.append("</div>\r\n");
	}
	
	public static void create(List<ProcedureHtmlTO> codes, String schemaName) {
		String fileCount = "<p>模式名：%s</p>\r\n";

		StringBuilder html = new StringBuilder();
		htmlInit(html);//html
		setHead(html);//head

		//add body
		html.append("<body>\r\n")
			.append("<div class=\"container\">")
			.append(String.format(fileCount, schemaName));
		
		//open dialog to show data when converter fail function 
		htmlOpenDialog(html);

		//sum table
		StringBuilder children = sumTable(codes, html);
		
		//p
		html.append("<p>点击数字显示详情</p>\r\n");//p

		//Add children table
		html.append(children.toString());
		
		html.append("</div>");
		
		//add script
		setScript(html);
		
		//end
		html.append("</body>\r\n</html>\r\n");

		//write file
		String fileName = "converterfile" +  File.separator + System.currentTimeMillis() + ".html";
		try {
			FileOutputStream writerStream = new FileOutputStream(fileName);    
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(writerStream, "UTF-8"));
			out.write(html.toString());
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

            try {
                java.awt.Desktop.getDesktop().open(new File(fileName));
            } catch (IOException ex) {
                Logger.getLogger(HtmlUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
	}

	public static void createSub(String text, String objectName) {
		
		StringBuffer html = new StringBuffer();
		html.append("<!DOCTYPE html>\r\n")
			.append("<html lang=\"en\">\r\n")
			.append("<meta charset=\"UTF-8\">\r\n")
			.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">\r\n")
			.append("<head>\r\n")
			.append("<body>\r\n")
			.append("<textarea rows=\"50\" cols=\"160\" readonly=\"readonly\">");
		
		html.append(text);
		
		html.append("</textarea>")
			.append("</body>")
			.append("</html>");
		
		File htmlDir = new File("converterfile" +  File.separator + "html" );
		if(!htmlDir.exists()) {
			htmlDir.mkdirs();
		}
		
		String fileName = "converterfile" +  File.separator + "html" + File.separator + objectName + ".html";
		try {
			FileOutputStream writerStream = new FileOutputStream(fileName);    
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(writerStream, "UTF-8"));
			out.write(html.toString());
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void createHtml(List<FunctionTO> codes, String FileName, int count, boolean isbuild) {
		
		StringBuilder html = new StringBuilder();
		htmlInit(html);//html
		setHead(html);//head

		//add body
		html.append("<body>\r\n")
			.append("<div class=\"container\">")
			.append("编译评估结果：");
		
		//open dialog to show data when converter fail function 
		htmlOpenDialog(html);

		//sum table
		StringBuilder children = sumBuildFunctionTable(codes, count, html, isbuild);
		
		//p
		//html.append("<p>点击数字显示详情</p>\r\n");//p

		//Add children table
		html.append(children.toString());
		
		html.append("</div>");
		
		//add script
		setScript(html);
		
		//end
		html.append("</body>\r\n</html>\r\n");

		//write file
		String fileName = "converterfile" +  File.separator + FileName + ".html";
		try {
			FileOutputStream writerStream = new FileOutputStream(fileName);    
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(writerStream, "UTF-8"));
			out.write(html.toString());
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

            try {
                java.awt.Desktop.getDesktop().open(new File(fileName));
            } catch (IOException ex) {
                Logger.getLogger(HtmlUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
	}

	private static StringBuilder sumBuildFunctionTable(List<FunctionTO> errorList, int count, StringBuilder html, boolean isbuild) {
		StringBuilder children = new StringBuilder();
		
		html.append("<div class=\"row\">\r\n")
			.append("	<div class=\"col-md-10\">\r\n")
			.append("		<div class=\"panel panel-default\">\r\n")
			.append("			<div class=\"panel-heading\">统计信息</div>\r\n");
		
		html.append("<table class=\"table\" style=\"text-align: center\">\r\n")
			.append("<thead>\r\n")
			.append("	<tr>\r\n")
			.append("	<td width='55px'>序号</td>\r\n") 
			.append("	<td width='150px'>类型</td>\r\n");
		if(isbuild) {
			html.append("	<td width='100px'>成功数</td>\r\n");
		}
		html.append("	<td width='100px'>失败数</td>\r\n");
		if(isbuild) {
			html.append("	<td width='100px'>合计</td>\r\n"); 
		} else {
			html.append("	<td width='100px'>问题数</td>\r\n"); 
		}
		html.append("<thead>\r\n");
			
		html.append("<tbody>\r\n")
			.append("	<tr>\r\n")
			.append("		<td>").append(1).append("</td>\r\n")
			.append("		<td>").append(Plsqlconverter.OBJECT_TYPE_FUNCTION).append("</td>\r\n");
		if(isbuild) {
			html.append("		<td>").append(count - errorList.size()).append("</td>\r\n");
		}
		html.append("		<td>").append(errorList.size()).append("</td>\r\n");
		
		//for children table
		int errorCount = buildFunctionChildrenTable(errorList, children);
		
		if(isbuild) {
			html.append("		<td>").append(count).append("</td>\r\n");
		} else {
			html.append("		<td>").append(errorCount).append("</td>\r\n");
		}
		html.append("	</tr>\r\n")
			.append("</tbody>\r\n");
			
		html.append("</table>\r\n");
		
		html.append("		</div>\r\n")
			.append("	</div>\r\n")
			.append("</div>\r\n");
		
		return children;
	}

	private static int buildFunctionChildrenTable(List<FunctionTO> errorList, StringBuilder children) {

		int errorCount = 0;
		//Add thead 
		children.append("<div class=\"row\" name=\"content1\">\r\n")
			.append("	<div class=\"col-md-12\">\r\n")
			.append("		<div class=\"panel panel-default\">\r\n")
			.append("<div class=\"panel-heading\">" + "编译错误详细信息:"  +  "</div>")
			.append("<table class=\"table\" style=\"text-align: center\">")
			.append("<thead>\r\n")
			.append("	<tr>\r\n");

		children.append("<td></td>\r\n")
			.append("	<td width='55px'>序号</td>\r\n") 
			.append("	<td width='150px'>模式</td>\r\n")
			.append("	<td width='400px'>名称</td>\r\n")
			.append("	<td>信息</td>\r\n")
			.append("<thead>\r\n");
		
		//Add tbody
		for(int i=0;i<errorList.size();i++) {
			children.append("<tbody>\r\n")
			.append("	<tr>\r\n");

			children.append("<td><span class=\"glyphicon glyphicon-plus\" id=\"" + errorList.get(i).getFunctionName() + i +"\" \r\n")
					.append("	style=\"cursor: pointer\"\r\n")
					.append("	onclick=\"openModal(this,'"+ errorList.get(i).getFunctionName() + "')\" \r\n")
					.append("	xid=\"ERP_DRP_MATERIALSFAMILYIMORG\"></span></td>\r\n");

			children.append("		<td>").append(i+ 1).append("</td>\r\n")
			.append("		<td>").append(errorList.get(i).getSchemaName()).append("</td>\r\n") 
			.append("		<td>").append(errorList.get(i).getFunctionName()).append("</td>\r\n");
			String message = "";
			for(String msg :errorList.get(i).getErrorMsgList()) {
				message = message + msg + "<br>";
				errorCount++;
			}
			children.append("		<td style=\"word-wrap:break-word;word-break:break-all;\" align=\"left\">").append(message).append("</td>\r\n")
			.append("	</tr>\r\n")
			.append("</tbody>\r\n");
		}
		children.append("</table>\r\n")
				.append("</div>")
				.append("</div>")
				.append("</div>");

		return errorCount;
		
	}
	
}
