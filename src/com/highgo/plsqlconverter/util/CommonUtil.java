/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.highgo.plsqlconverter.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author yanggq
 */
public class CommonUtil {
    
    /*
    * Converter type to pg from oracle 
    */
    public static String typeOracle2Pg(String oracleType){
        
        if (oracleType.toUpperCase().startsWith("REF CURSOR")) {
            return "REFCURSOR";
        }
        
        if("record".equalsIgnoreCase(oracleType)) {
            return "record";
        }
        
        return oracleType;
    }
    
    /*
    * replace value to replaceValue im text
    */
    public static String replaceText(String text, String value, String replaceValue){
        Pattern patternType = Pattern.compile("[^\\w+]" + value + "([^\\w+])", Pattern.CASE_INSENSITIVE);
        Matcher matcherType = patternType.matcher(text);
        StringBuffer temp = new StringBuffer();
        int start = 0;
        int end = 0;
        while(matcherType.find()) {
            String endValue = matcherType.group(matcherType.groupCount());
            temp.append(text.substring(start, matcherType.end() - value.length() - endValue.length()));
            temp.append(replaceValue);
            temp.append(text.substring(matcherType.end() - endValue.length(), matcherType.end()));
            start = matcherType.end();
            end = matcherType.end();
        }
        if(start != 0) {
            temp.append(text.substring(end));
            text = temp.toString();
        }
        return text;
    }
    
        /*
    *Convert dollar sign
    */
    public static String convertDollarSign(String str) {
    	Pattern pattern = Pattern.compile("'.*(\\$\\$)(.*)(\\$\\$).*'");
    	Matcher matcher = pattern.matcher(str);

        StringBuffer sb = new StringBuffer();
        int end = 0;
    	while(matcher.find()) {
            sb.append(str.substring(end, matcher.start()));
            sb.append(" E ");
            sb.append(str.substring(matcher.start(), matcher.start(1)));
            sb.append("\\$\\$");
            sb.append(matcher.group(2));
            sb.append("\\$\\$");
            sb.append(str.substring(matcher.end(3), matcher.end()));
            end = matcher.end();
    	}
        
        sb.append(str.substring(end));

        return sb.toString();
    }
    
    /* 
    * replace table(functionName) to function
    * * select * fromt table(function()) => select * from function()
    */
//    public static String repalceTable(String text){
//         String rnt  = text;
//
//         Pattern patternType = Pattern.compile("\\s+(table\\s*\\(\\s*)", Pattern.CASE_INSENSITIVE);
//         Matcher matcherType = patternType.matcher(rnt);
//         if(matcherType.find()) {
//             String temp = rnt.substring(0, matcherType.start());
//             String strEndValue = rnt.substring(matcherType.end());
//
//             Pattern pattern2 = Pattern.compile("\\)", Pattern.CASE_INSENSITIVE);
//             Matcher matcher2 = pattern2.matcher(strEndValue);
//             int count = 0;
//             while(matcher2.find()) {
//                 count++;
//
//                 String strleft = strEndValue.substring(0, matcher2.end());
//
//                 int leftcount = appearNumber(strleft, '(');
//                 if((leftcount + 1) == count) {
//                         strEndValue = strEndValue.substring(0, matcher2.end() - 1) + strEndValue.substring(matcher2.end());
//                         break;
//                 }
//             }
//
//             rnt = temp + " " + strEndValue;
//
//             return repalceTable(rnt);
//         }
//
//         return rnt;
//     }
    
    /*
    * Get the count of key in text
    */
//    private static int appearNumber(String text, char key){
//        char[] chars = text.toCharArray();
//        int count = 0;
//        for(char c : chars) {
//            if(c == key) {
//                count++;
//            }
//        }
//        return count;
//    }
    
    public static String updateChineseChar(String text){
        text = text.replaceAll("\uff0c", ","); //Semicolon
        text = text.replaceAll("\uff08", "(");//Left parenthesis
        text = text.replaceAll("\uff09", ")");//right parenthesis
        return text;
    }
    
    public static String handBeforConverter(String text){
        text = changeCommentFormat(text);
        text = handChineseCharOutCommant(text);
        
        text = handAuthidCurrentUser(text);

        return text;
    }
    
    private static String handAuthidCurrentUser(String input) {
        String text = input;
        StringBuffer value = new StringBuffer();
        
        Pattern pattern = Pattern.compile("[^\\w]Authid\\s+Current_User[^\\w]", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
        int end = 0;

        while(matcher.find()) {
            if(end > 0) {
                end = end - 1;
            }
            String temp = text.substring(end, matcher.start() + 1);
            value.append(temp);

            end = matcher.end();
        }
        
        if(end > 0) {
            end = end - 1;
        }
        value.append(text.substring(end));
        
        return value.toString();
    }
    
     /*
     * change comment format
     */
     private static String changeCommentFormat(String str){
        StringBuffer strBuff = new StringBuffer(str);
        int flag = 0;
        for(int i = 0;i < strBuff.length() - 1;i++) {
            if(strBuff.charAt(i) == '/' && strBuff.charAt(i+1) =='*') {
                    if(flag == 0) {
                            flag = 1;
                    }else if(flag == 1){
                            strBuff.insert(i-1,"*/",0,2);
                            flag = 0;
                    }
            } else if(strBuff.charAt(i) == '*' && strBuff.charAt(i+1) =='/' && flag == 1) {
                    flag = 0;
            }
        }
        return strBuff.toString();
     }
    
    private static String handChineseCharOutCommant(String input){
        String text = input;
        StringBuffer value = new StringBuffer();
        while(true) {
            Pattern pattern = Pattern.compile("'|(\\/\\*)|(--)", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(text);
            int end = 0;
            
            String endValue ="";

            if(matcher.find()) {
                String temp = text.substring(end, matcher.start());
                temp = updateChineseChar(temp);
                value.append(temp);
                String strStart = text.substring(matcher.start(), matcher.end());
                //value.append(strStart);
                if(strStart.equals("'")) {
                        endValue = getfullChar(text.substring(matcher.start()), value);
                        text = endValue;
                        continue;
                        //endValue = handChineseCharOutCommant(endValue);
                } else if("/*".equals(strStart)) {
                        endValue = getCommont(text.substring(matcher.start()), value);
                        text = endValue;
                        continue;
                        //endValue = handChineseCharOutCommant(endValue);
                } else if("--".equals(strStart)){
                        endValue = getCommont2(text.substring(matcher.start()), value);
                        text = endValue;
                        continue;
                        //endValue = handChineseCharOutCommant(endValue);
                }

                end = matcher.end();
                value.append(endValue);
            } else {
                text = updateChineseChar(text);
                value.append(text);
                break;
            }
        }
        
        return value.toString();
    }
    
    private static String getCommont2(String text, StringBuffer sb) {
	Pattern pattern = Pattern.compile("\\n|\\r", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);

        if(matcher.find()) {
            sb.append(text.substring(0, matcher.end()));

            return text.substring(matcher.end());
        } 
        return "";
    }
    
    private static String getCommont(String text, StringBuffer sb) {
        Pattern pattern = Pattern.compile("\\*\\/", Pattern.CASE_INSENSITIVE);
	Matcher matcher = pattern.matcher(text);
	    
        if(matcher.find()) {
            sb.append(text.substring(0, matcher.end()));

            return text.substring(matcher.end());
        } 
            return null;
    }
    
    private static String getfullChar(String text, StringBuffer sb) {
        Pattern pattern = Pattern.compile("('+)", Pattern.CASE_INSENSITIVE);
	Matcher matcher = pattern.matcher(text);
	    
        int count = 0;
        int start = 0;
        int end = 0;
        
        String temp = "";
        
        while(matcher.find()) {
            count = count + matcher.group(1).length();
            if(start == 0) {
                    start = matcher.start();
                    temp = text.substring(end, start);
                    //temp = updateChineseChar(temp);
                    sb.append(temp);
            }
        	
            if(count % 2 == 0) {
                end = matcher.end();
                sb.append(text.substring(start, end));
                return text.substring(end);
            }
        }
        
	return temp;
    }
    
    /*
    * insert SQL add character as
    *insert into tablename v(v.column1 , v.column2 , v.column3)；
    * => insert into tablename as v(v.column1 , v.column2 , v.column3)；
    */
    public static String insertIntoSqladdas(String text){
        Pattern pattern = Pattern.compile("insert\\s+into\\s+(\\s*\\/\\*[\\s\\S]*?\\*\\/\\s*)*\\w+\\s+(\\w+\\s*)\\(", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
        StringBuffer temp = new StringBuffer();
        int start = 0;
        int end = 0;
        while(matcher.find()) {
            String endValue = matcher.group(2);
            if("values".equalsIgnoreCase(endValue.trim())) {
               continue; 
            }

            temp.append(text.substring(start, matcher.end() - endValue.length() - 1));
            temp.append("as ");
            temp.append(text.substring(matcher.end() - endValue.length() - 1, matcher.end()));
            start = matcher.end();
            end = matcher.end();
        }
        
        temp.append(text.substring(end));
        text = temp.toString();
        
        return text;
    }
    
//    public static String changeDoubleDollarChar(String text){
//        Pattern pattern = Pattern.compile("(\\s+)\\$\\$(\\w+)\\$\\$(\\s+)", Pattern.CASE_INSENSITIVE);
//        Matcher matcher = pattern.matcher(text);
//        
//        StringBuffer temp = new StringBuffer();
//        int end = 0;
//        while(matcher.find()) {
//            String before = matcher.group(1);
//            String value = matcher.group(2);
//            String after = matcher.group(3);
//
//            temp.append(text.substring(end, matcher.start()));
//            temp.append(before);
//            temp.append("E ");
//            temp.append("\\$\\$");
//            temp.append(value);
//            temp.append("\\$\\$");
//            temp.append(after);
//            
//            end = matcher.end();
//        }
//        
//        temp.append(text.substring(end));
//        text = temp.toString();
//        return text; 
//    }

}
