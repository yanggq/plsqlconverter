/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.highgo.plsqlconverter.util;

import com.highgo.plsqlconverter.model.ConnInfoTO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author yanggq
 */
public class JdbcHelper {
    
    public enum DBType{
        ORACLE,
        POSTGRESQL
    }
    
    public static Connection getConnection(DBType type, ConnInfoTO connInfo) throws ClassNotFoundException, SQLException{
        Connection conn = null;
        String url = null;
        switch (type) {
            case ORACLE:
                Class.forName("oracle.jdbc.driver.OracleDriver");//better use oracle.jdbc.OracleDriver after v9
                url = "jdbc:oracle:thin:@" + connInfo.getHost() + ":" + connInfo.getPort() + "/" + connInfo.getServerName();
                conn = DriverManager.getConnection(url, connInfo.getUserName(), connInfo.getPassword());

                return conn;
            case POSTGRESQL:
                Class.forName("org.postgresql.Driver");//better use oracle.jdbc.OracleDriver after v9
                url = "jdbc:postgresql://" + connInfo.getHost() + ":" + connInfo.getPort() + "/" + connInfo.getDbName();
                conn = DriverManager.getConnection(url, connInfo.getUserName(), connInfo.getPassword());

                return conn;
        }
        
        return conn;
    }
    
    public static void close(Connection conn){
        try {
            if(conn != null) {
            
                conn.close();
                conn = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(JdbcHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
