/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.highgo.plsqlconverter.model;

/**
 *
 * @author yanggq
 */
public class ProcedureInfoTO {
    private String objectName;
    private String objectType;
    private String objectId;
    private String objectText;
    private String objectOwner;
    private String converterText;
    private boolean converterFlg;
    private boolean buildError;
    private String message;

    public ProcedureInfoTO(String objectName, String objectType, String objectId, String objectOwner) {
        this.objectName = objectName;
        this.objectType = objectType;
        this.objectId = objectId;
        this.objectOwner = objectOwner;
    }
    
    public String[] getShow(){
        String[] rnt = new String[2];
        rnt[0] = this.objectName;
        rnt[1] = this.objectType;
                
        return rnt;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getObjectText() {
        return objectText;
    }

    public void setObjectText(String objectText) {
        this.objectText = objectText;
    }

    public String getObjectOwner() {
        return objectOwner;
    }

    public void setObjectOwner(String objectOwner) {
        this.objectOwner = objectOwner;
    }

    public String getConverterText() {
        return converterText;
    }

    public void setConverterText(String converterText) {
        this.converterText = converterText;
    }

    public boolean isConverterFlg() {
        return converterFlg;
    }

    public void setConverterFlg(boolean converterFlg) {
        this.converterFlg = converterFlg;
    }

    public boolean isBuildError() {
        return buildError;
    }

    public void setBuildError(boolean buildError) {
        this.buildError = buildError;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
    
    
}
