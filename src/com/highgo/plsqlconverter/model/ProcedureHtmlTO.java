/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.highgo.plsqlconverter.model;

import java.util.List;

/**
 *
 * @author yanggq
 */
public class ProcedureHtmlTO {
    private String typeName;
    private int successCount;
    private int failCount;
    private int buildErrorCount;
    private List<ProcedureInfoTO> successList;
    private List<ProcedureInfoTO> failList;
    private List<ProcedureInfoTO> buildErrorList;

    public int getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(int successCount) {
        this.successCount = successCount;
    }

    public int getFailCount() {
        return failCount;
    }

    public void setFailCount(int failCount) {
        this.failCount = failCount;
    }

    public List<ProcedureInfoTO> getSuccessList() {
        return successList;
    }

    public void setSuccessList(List<ProcedureInfoTO> successList) {
        this.successList = successList;
    }

    public List<ProcedureInfoTO> getFailList() {
        return failList;
    }

    public void setFailList(List<ProcedureInfoTO> failList) {
        this.failList = failList;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getBuildErrorCount() {
        return buildErrorCount;
    }

    public void setBuildErrorCount(int buildErrorCount) {
        this.buildErrorCount = buildErrorCount;
    }

    public List<ProcedureInfoTO> getBuildErrorList() {
        return buildErrorList;
    }

    public void setBuildErrorList(List<ProcedureInfoTO> buildErrorList) {
        this.buildErrorList = buildErrorList;
    }

}
