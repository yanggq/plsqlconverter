/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.highgo.plsqlconverter.model;

/**
 *
 * @author yanggq
 */
public class ConnInfoTO {
    
    private String host;
    private int port;
    private String userName;
    private String password;
    private String serverName;
    private String dbName;
    
    public ConnInfoTO(){ 
    }

    public ConnInfoTO(String host, int port, String userName, String password, String serverName, String dbName) {
        this.host = host;
        this.port = port;
        this.userName = userName;
        this.password = password;
        this.serverName = serverName;
        this.dbName = dbName;
    }
    
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
    
    
    
}
